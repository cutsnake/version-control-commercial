﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using AI;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class FoxScript : MonoBehaviour
{
    public float Speed;
    private float Distances;
    public List<GameObject> Animals;
    public GameObject CountReference;
    public Boolean IsWithindog;
    public GameObject Dog;
    public float movespeed = 3f;
    public float rotspeed = 100f;
    public bool iswandering = false;
    public bool isrotatingleft = false;
    public bool isrotatingright = false;
    public bool iswalking = false;
	public GameObject curTarget;
	public float TargetCheck = 0.5f;
    


    // Use this for initialization
    void Start()
    {

        CountReference = GameObject.Find("AnimalCounter");
		Dog = GameObject.Find("sheepdog");
        Animals = new List<GameObject>(GameObject.FindGameObjectsWithTag("AllAnimals"));


        //Transform closest = Animals[0];
        //float dist = Vector3.Distance(Animals[0].position, transform.position);

        for (int i = 0; i < Animals.Count; i++)
        {
            for (int j = 1; j < Animals.Count; j++)
            {
                /*float compare = Vector3.Distance(transform.position, closest.position);
                if (compare < dist)
                {
                    closest = Animals[i];
                    dist = compare;
                }*/
                float disti = Vector3.Distance(transform.position, Animals[i].transform.position);
                float distj = Vector3.Distance(transform.position, Animals[j].transform.position);

                if (distj < disti)
                {
                    GameObject temp = Animals[j];
                    Animals[j] = Animals[i];
                    Animals[i] = temp;
                }
            }
        }
		StartCoroutine(targetlocation());
	}

    // Update is called once per frame
    void Update()
    {
		if(curTarget == null)
		{
			StopCoroutine(targetlocation());
			StartCoroutine(targetlocation());
		}
		else
		{
			//Chase the target if it's close enough
			Distances = Vector3.Distance(transform.position, curTarget.transform.position);
			if (Distances <= 10f)
			{
				float step = Speed * Time.deltaTime;
				transform.position = Vector3.MoveTowards(transform.position, curTarget.transform.position, step);
				Speed = 5f;
			}
		}
		IgnoreSheepDog();



       
    }

	public void FindNewTarget()
	{
		//Grab dog's list, make a list of possible targets from the scene animals NOT in that list
		/*List<Animal> dogList = Dog.GetComponent<DogFlock>().AnimalsInFlock;
		List<GameObject> possibleTargets = new List<GameObject>();

		for(int i = 0; i < Animals.Count; i++)
		{
			if(!dogList.Contains(Animals[i].GetComponent<Animal>()))
			{
				possibleTargets.Add(Animals[i]);
			}
		}

		//Find the closest target
		float targetDist = 100;
		int targetIndex = 0;
		for(int i = 0; i < possibleTargets.Count; i ++)
		{
			float val = Vector3.Distance(transform.position, possibleTargets[i].transform.position);
			if(val < targetDist)
			{
				targetDist = val;
				targetIndex = i;
			}
		}
		curTarget = possibleTargets[targetIndex];*/
	}

	IEnumerator targetlocation()
	{
		while (true)
		{
			yield return new WaitForSeconds(TargetCheck);
			List<Animal> dogList = Dog.GetComponent<DogFlock>().AnimalsInFlock;
			List<GameObject> possibleTargets = new List<GameObject>();

			for (int i = 0; i < Animals.Count; i++)
			{
				if (!dogList.Contains(Animals[i].GetComponent<Animal>()))
				{
					possibleTargets.Add(Animals[i]);
				}
			}

			//Find the closest target
			float targetDist = 100;
			int targetIndex = 0;
			for (int i = 0; i < possibleTargets.Count; i++)
			{
				float val = Vector3.Distance(transform.position, possibleTargets[i].transform.position);
				if (val < targetDist)
				{
					targetDist = val;
					targetIndex = i;
				}
			}
			curTarget = possibleTargets[targetIndex];
		}

	}

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("AllAnimals"))
        {
			/*if (Animals.Contains(other.gameObject))
            {
                
            }*/
			StopCoroutine(targetlocation());

			if (Dog.GetComponent<DogFlock>().AnimalsInFlock.Contains(other.gameObject.GetComponent<Animal>()))
			{
				Dog.GetComponent<DogFlock>().AnimalsInFlock.Remove(other.gameObject.GetComponent<Animal>());
			}
			Animals.Remove(other.gameObject);
	        
	        Animal ThisAnimal = other.gameObject.GetComponent<Animal>();
	        ThisAnimal.RequestDestroy(true);
	       
            CountReference.GetComponent<UIScript>().updateUiCount();
          //  print("hit");

			StartCoroutine(targetlocation());
		}
    }

    public List<GameObject> Getlist()
    {
        return Animals;
    }

    public void IgnoreSheepDog()
    {
        if (iswandering == false)
        {
            StartCoroutine(Wander());
        }

        if (isrotatingright == true)
        {
            transform.Rotate(transform.up * Time.deltaTime * rotspeed);
        }
        
        if (isrotatingleft == true)
        {
            transform.Rotate(transform.up * Time.deltaTime * -rotspeed);
        }

        if (iswalking == true)
        {
            transform.position += transform.forward * movespeed * Time.deltaTime; 
        }
    }

    IEnumerator Wander()
    {
		
        int rotTime = Random.Range(1, 3);
        int Rotatewait = Random.Range(1, 4);
        int RotateLorR = Random.Range(1, 2);
        int walkWait = Random.Range(1, 4);
        int walkTime = Random.Range(1, 5);

        iswandering = true;
        
        yield return new WaitForSeconds(walkWait);
        iswalking = true;
        yield return new WaitForSeconds(walkTime);
        iswalking = false; 
        yield return new WaitForSeconds(Rotatewait);
        if (RotateLorR == 1)
        {
            isrotatingright = true;
            yield return new WaitForSeconds(rotTime);
            isrotatingright = false;
            
        }
        if (RotateLorR == 2)
        {
            isrotatingleft = true;
            yield return new WaitForSeconds(rotTime);
            isrotatingleft = false;
            
        }

        iswandering = false; 
    }

	private void OnDrawGizmos()
	{
		if (curTarget != null)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(curTarget.transform.position, 2.0f);
		}
	} 
}