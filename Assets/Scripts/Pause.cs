﻿using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 using UnityEngine.SceneManagement;
 
 public class Pause : MonoBehaviour
 {
     public GameObject PausePanel; 
     
     // Use this for initialization
     void Start()
     {
         
     }
 
     // Update is called once per frame
     void Update()
     {
         
     }
 
     public void restart()
     {
         SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
         Time.timeScale = 1;
 
     }
 
     public void Resume()
     {
         Time.timeScale = 1;
         PausePanel.SetActive(false);
     }
 
     public void Exit()
     {
         Application.Quit();
     }
 
     public void pause()
     {
         Time.timeScale = 0; 
         PausePanel.SetActive(true);
         
     }
 }