﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovePlayer : MonoBehaviour
{
    public NavMeshAgent nmAgent;
    public GameObject selectedObject;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))
            {
                nmAgent.SetDestination(hit.point);
            }
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit HitInfo;


        if (Physics.Raycast(ray, out HitInfo))
        {
            Debug.Log("mouse is over" + HitInfo.collider.name);
            GameObject hitObject = HitInfo.transform.root.gameObject;
        }
        else
        {
        }
    }

    void selectObject(GameObject obj)
    {
        if (selectedObject != null)
        {
            if (obj == selectedObject)
            {
                return;

                ClearSelction();
            }

            selectedObject = obj;
            Renderer r = selectedObject.GetComponent<Renderer>();
            Material m = r.material;
            m.color = Color.green;
            r.material = m; 
        }
    }

    void ClearSelction()
    {
        selectedObject = null;
    }
}