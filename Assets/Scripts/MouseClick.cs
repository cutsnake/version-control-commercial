﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseClick : MonoBehaviour {
    public Text chatBox;
    private GameObject text;
	public GameObject countdown;
    public Text charaName;
    public RaycastHit hit;
    public Text mission;
	public GameObject passport;
	public Sprite bedroom;
	public Sprite airport;
	public Sprite beach;
    public Sprite Freya;
    public Sprite Bryn;
    public SpriteRenderer myScene;
    public SpriteRenderer myCharacter;
	public int sceneText;
    public Transform SpawnLocation;

    public float timer;
    public Text timerSeconds;

    //Image myScene;
    //ublic GameObject yourPhone;
    // Use this for initialization
    void Start ()
    {
        text = GameObject.Find("/Canvas/Image/CurrentScene/Chat");
        //utton btn = yourPhone.GetComponent<Button>();
        //GetComponent(SpriteRenderer).sprite = myScene;
		sceneText = 0;
     //   mission.text = "Press the button and roll.";
     //   chatBox.text = "";
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (sceneText == 4)
        {




            timer -= Time.deltaTime;
            timerSeconds.text = timer.ToString("f1");

            if (timer <= 0)
            {
                timer = 0;
                timerSeconds.text = "Game Over";

            }
        }

            //text. = "Found it!";
            if (Input.GetMouseButtonDown(0))
        {

           
                if (sceneText == 0)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "What are you doing Freya! You are going to be late.";
                charaName.text = "Bryn";
                myCharacter.sprite = Bryn;
                myScene.sprite = bedroom;
                sceneText++;
            }
            else if (sceneText == 1)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "Oh no. Gotta hurry...";
                charaName.text = "Freya";
                myCharacter.sprite = Freya;
                myScene.sprite = bedroom;
                sceneText++;
            }
            else if (sceneText == 2)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "Wait...where's my passport?";

                sceneText++;
       
            }
            else if (sceneText == 3)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                chatBox.text = "Now where could it be...";
                mission.text = "Find your passport!";
                timer = 60;
                timer -= Time.deltaTime;
                timerSeconds.text = timer.ToString("f1");

                sceneText++;

            }
            else if (sceneText == 4) 
			{

				var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
                //if (Physics.Raycast(ray, out hit))
               // Debug.Log("Name = " + hit.collider.name);
                //Debug.Log("Tag = " + hit.collider.tag);
                //Debug.Log("Hit Point = " + hit.point);
                //Debug.Log("Object position = " + hit.collider.gameObject.transform.position);
                //Debug.Log("--------------");
                    chatBox.text = "Found it!";
                    charaName.text = "Freya";
                    mission.text = "Clear!";
                    timerSeconds.text = "";
                  
                    Instantiate(passport, (SpawnLocation.position), SpawnLocation.rotation);
                    sceneText++;
                
			}
			else if (sceneText == 5) 
			{
				var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				chatBox.text = "I'm coming Bryn!";
				charaName.text = "Freya";
				mission.text = "";
                //myScene.sprite = airport;

                sceneText++;
			}
            else if (sceneText == 6)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "Hurry!";
                charaName.text = "Bryn";
                myCharacter.sprite = Bryn;
                sceneText++;
            }
            else if (sceneText == 7)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "I made it.";
                charaName.text = "Freya";
                myCharacter.sprite = Freya;
                myScene.sprite = airport;
                sceneText++;
            }
            else if (sceneText == 8)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "Do you want to sleep or check out this magazine?";
                charaName.text = "Bryn";
                myCharacter.sprite = Bryn;
                myScene.sprite = airport;
                sceneText++;
            }
            else if (sceneText == 9)
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                chatBox.text = "Sleep\n Read";
                charaName.text = "Freya";
                myCharacter.sprite = Freya;
                myScene.sprite = airport;
                sceneText++;
            }


            //          if (hit.collider.gameObject.tag == "Phone")
            //          {
            //              chatBox.text = "Found it!";
            //              charaName.text = "Freya";
            //          }
        }
    }
    private void OnMouseDown()
    {
        Destroy(gameObject);
    }

}
