﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPad : MonoBehaviour
{

	public int Code;

	private void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.name == "player")
		{
			foreach (TeleportPad tp in FindObjectsOfType<TeleportPad>())
			{
				if (tp.Code == Code && tp != this)
				{
					Vector3 postion = tp.gameObject.transform.position;
					postion.y += 2;
					collider.gameObject.transform.position = postion;
				}
			}
		}
	}
}
