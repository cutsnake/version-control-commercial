﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
	//public GameObject CurrentSelection;
	public GameObject selectedObjects; 
	

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetMouseButtonDown(0))
		{
			OnMouseDown();
            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //RaycastHit hitInfo;


            //if (Physics.Raycast(ray, out hitInfo))
            //{
            //    GameObject hitobject = hitInfo.transform.root.gameObject;

            //    Debug.Log(("Mouse is over:" + hitobject.name));

            //    SelectObject(hitobject);
            //}
            //else
            //{
            //    ClearSelection();
            //}
        }
		
		
			
		
		
		
		
	}

	private void OnMouseDown()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		

		if (Physics.Raycast(ray, out hitInfo))
		{
			GameObject hitobject = hitInfo.transform.root.gameObject;
			
			Debug.Log(("Mouse is over:"+ hitobject.name));

			SelectObject(hitobject);
		}
		else
		{
			ClearSelection();
		}
	}

	void SelectObject(GameObject obj)
	{
		if (selectedObjects != null)
		{
			if (obj == selectedObjects)
			{
				Debug.Log("Clear");
				ClearSelection();
				return;
			}
			else
			{
				ClearSelection();
				//adding to list would stil go here
				selectedObjects = obj;
				Renderer[] rs = selectedObjects.GetComponentsInChildren<Renderer>();
				foreach (Renderer r in rs)
				{
					Debug.Log(r.name);
					Material m = r.material;
					m.color = Color.white;
					r.material = m;
				}
			}
		}
		else
		{
			selectedObjects = obj;
			Renderer[] rs = selectedObjects.GetComponentsInChildren<Renderer>();
			foreach(Renderer r in rs)
			{
				Debug.Log(r.name);
				Material m = r.material; 
				m.color = Color.white;
				r.material = m; 
			}
		}
	}

	void ClearSelection()
	{ 
		
		Renderer[] rs = selectedObjects.GetComponentsInChildren<Renderer>();
		
		foreach (Renderer r in rs)
		{
			Material m = r.material; 
			m.color = Color.green;
			r.material = m; 
		}
            selectedObjects = null;
	}


	void MoveOnSecondClick()
	{
		
		
			
		
	}
	

}
