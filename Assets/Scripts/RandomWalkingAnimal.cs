﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class RandomWalkingAnimal : MonoBehaviour
{
    public float Speed;

    public float Timeer;
    public bool TimerReset;


    // Use this for initialization
    void Start()
    {
        float Step = Speed * Time.deltaTime;
        
    }

    // Update is called once per frame
    void Update()
    {
        Timeer += Time.deltaTime;
        
        if (Timeer >= 5f)
        {
            TimerReset = true;
            RunTimeSequence();
            print(Timeer);
        }

        if (TimerReset == true)
        {
            Timeer = 0f;
        }
    }


    void RunTimeSequence()
    {
        transform.localRotation = Quaternion.Euler(0, Random.Range(1, 360), 0);
        GetComponent<Rigidbody>().AddRelativeForce(15, 0, 0);
    }
}