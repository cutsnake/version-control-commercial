﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigTeleport : MonoBehaviour {

    public Transform target;
    Vector3 distance;
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Teleport")
        {
            this.transform.position = target.position;
        }
    }

}
