﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class AllAnimalWander : MonoBehaviour
{
    public float movespeed = 3f;
    public float rotspeed = 100f;
    public bool iswandering = false;
    public bool isrotatingleft = false;
    public bool isrotatingright = false;

    public bool iswalking = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        IsWalking();
    }

    public void IsWalking()
    {
        if (iswandering == false)
        {
            StartCoroutine(Wandering());
        }

        if (isrotatingright == true)
        {
            transform.Rotate(transform.up * Time.deltaTime * rotspeed);
        }

        if (isrotatingleft == true)
        {
            transform.Rotate(transform.up * Time.deltaTime * -rotspeed);
        }

        if (iswalking == true)
        {
            transform.position += transform.forward * movespeed * Time.deltaTime;
        }
    }

    IEnumerator Wandering()
    {
        int rotTime = Random.Range(1, 3);
        int Rotatewait = Random.Range(1, 4);
        int RotateLorR = Random.Range(1, 2);
        int walkWait = Random.Range(1, 4);
        int walkTime = Random.Range(1, 5);

        iswandering = true;

        yield return new WaitForSeconds(walkWait);
        iswalking = true;
        yield return new WaitForSeconds(walkTime);
        iswalking = false;
        yield return new WaitForSeconds(Rotatewait);
        if (RotateLorR == 1)
        {
            isrotatingright = true;
            yield return new WaitForSeconds(rotTime);
            isrotatingright = false;
        }

        if (RotateLorR == 2)
        {
            isrotatingleft = true;
            yield return new WaitForSeconds(rotTime);
            isrotatingleft = false;
        }

        iswandering = false;
    }
}