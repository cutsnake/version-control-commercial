﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ondeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision other)
    {
        //If within certain distance from dog, nothing happens
        //else if
        if (other.gameObject.CompareTag("Fox"))
        {
            print("Game Over. Try Again.");
            // Load Scene or Bring up menu
           
        }
        // can also add other triggers that cause death
    }
}
