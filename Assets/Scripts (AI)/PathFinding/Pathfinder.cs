﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace AI
{
    public class Pathfinder
    {
        public NodeManager CurrentNM;

        public PathRequest CurrentPR;
        public Thread AvaliableThread;
        public PathFinderManager PFM;
        public PathfinderStatus CurrentStatus = PathfinderStatus.Incative;
        public int SkipDiagnalIndex = 0;

        private void Update()
        {

        }

        public void SubmitFindPath(PathRequest _PR, PathFinderManager _PFM, NodeManager _NM)
        {
            CurrentStatus = PathfinderStatus.Active;
            //Debug.Log("pf:  request recived by pathfinder!");

            CurrentPR = _PR;
            PFM = _PFM;
            if (CurrentPR == null)
            {
                //Debug.Log("PF:  Incompleate Request, Null PR!");
                CurrentStatus = PathfinderStatus.Incative;
                return;
            }

            if (CurrentPR.PathIsFound == true)
            {
                //Debug.Log("PF:  Path Request has already been solved!!");
                _PFM.CompleatedRequests.Enqueue(CurrentPR);
                CurrentStatus = PathfinderStatus.Incative;
                return;
            }

            if (CurrentPR.StartingNode == null || CurrentPR.TargetNode == null || CurrentPR.Requestee == null)
            {
                //Debug.Log("PF:  Incompleate Request, Null Node/Requetee!");
                CurrentStatus = PathfinderStatus.Incative;
                return;
            }
            else if (CurrentPR.IsBeingProcessed == false && CurrentPR.CompletedPath == null)
            {
                CurrentNM = _NM;
                CurrentPR.IsBeingProcessed = true;
                CallFindPath();
            }
        }

        //Comenses Path searching, sets to alternative thread and calls
        void CallFindPath()
        {
            AvaliableThread = new Thread(FindPath);
            AvaliableThread.Start();
        }

        //
        void FindPath()
        {

            System.Diagnostics.Stopwatch Watch = new System.Diagnostics.Stopwatch();
            Watch.Start();
            //Debug.Log("PF:  StartingPathFinding");
            Node StartNode = CurrentPR.StartingNode;
            Node TargetNode = CurrentPR.TargetNode;

            NodeHeap OpenSet = new NodeHeap((NodeManager.Instance.GridXLength * NodeManager.Instance.GridYLength));
            HashSet<Node> CloasedSet = new HashSet<Node>();
            OpenSet.Add(StartNode);

            while (OpenSet.Count() > 0)
            {
                Node CurrentNode = OpenSet.RemoveFirstNodeOnHeap();
                CloasedSet.Add(CurrentNode);

                if (CurrentNode == TargetNode)
                {
                    List<Node> RetracedPath = RetraceNodePath(StartNode, TargetNode);
                    CurrentPR.CompletedPath = RetracedPath;
                    CurrentPR.PathIsFound = true;
                    CurrentPR.IsBeingProcessed = false;
                    Watch.Stop();
                    //Debug.Log("Path Was Found in : " + Watch.ElapsedMilliseconds + "ms");
                    if (RetracedPath.Count == 0)
                        RetracedPath.Add(CurrentNode);
                    EjectPath();
                    break;
                }

                Node[] CurrentNodeNeighbours = CurrentNode.GetNeighbouringNodes();
                for (int NeighbourIndex = 0; NeighbourIndex < CurrentNodeNeighbours.Length; ++NeighbourIndex)
                {
                    if (CurrentNodeNeighbours[NeighbourIndex] == null)
                        continue;

                    Node NeighbourRef = CurrentNodeNeighbours[NeighbourIndex];

                    if (NeighbourRef.IsOccupied == true || CloasedSet.Contains(NeighbourRef))
                        continue;

                    int DistanceBetweenNodes = GetDistanceBetweenNode(CurrentNode, NeighbourRef);
                    int NewMovCostToNeighbour = CurrentNode.GCost + DistanceBetweenNodes;

                    if (NewMovCostToNeighbour - CurrentNode.GCost == 14)
                    {
                        if (HasSharedUnTreversableNode(CurrentNode, NeighbourRef))
                            continue;
                    }

                    if (NewMovCostToNeighbour < NeighbourRef.GCost || !OpenSet.Contains(NeighbourRef))
                    {
                        NeighbourRef.GCost = NewMovCostToNeighbour;
                        NeighbourRef.HCost = GetDistanceBetweenNode(NeighbourRef, TargetNode);
                        NeighbourRef.ParentNode = CurrentNode;

                        if (!OpenSet.Contains(NeighbourRef))
                        {
                            OpenSet.Add(NeighbourRef);
                        }
                    }
                }
            }
            CurrentPR.IsBeingProcessed = false;
            EjectPath();
        }


        #region Utility Methods

        //Retuns int (H cost) distance between two nodes
        int GetDistanceBetweenNode(Node _NodeA, Node _NodeB)
        {
            int DistX = (int)Mathf.Abs(_NodeA.GridPosition.x - _NodeB.GridPosition.x);
            int DistY = (int)Mathf.Abs(_NodeA.GridPosition.y - _NodeB.GridPosition.y);

            if (DistX > DistY)
                return 14 * DistY + 10 * (DistX - DistY);
            else
                return 14 * DistX + 10 * (DistY - DistX);
        }

        //Inverts the found Path order
        List<Node> RetraceNodePath(Node _StartNode, Node _EndNode)
        {
            List<Node> Path = new List<Node>();
            Node CurrentNode = _EndNode;

            while (CurrentNode != _StartNode)
            {
                Path.Add(CurrentNode);
                CurrentNode = CurrentNode.ParentNode;
            }

            Path.Reverse();
            return Path;
        }

        //Sends the PathRequest bac to PathFinderManager
        void EjectPath()
        {
            if (CurrentPR.PathIsFound == false)
            {
                //Debug.Log("PF:  Failed To Find Path");
                PFM.CompleatedRequests.Enqueue(CurrentPR);
            }
            else if (CurrentPR.PathIsFound == true)
            {
                //Debug.Log("PF:  Path Found");
                PFM.CompleatedRequests.Enqueue(CurrentPR);
                ResetPathFinder();
            }
        }

        //Resets the Pathfinder's local variables
        void ResetPathFinder()
        {
            CurrentPR = null;
            CurrentStatus = PathfinderStatus.Incative;
            AvaliableThread.Abort();
            AvaliableThread = null;
            CurrentNM = null;
            PFM = null;
        }

        bool HasSharedUnTreversableNode(Node _NodeA, Node _NodeB)
        {
            bool HasSharedUnTreversableNode = false;
            List<Vector2Int> VectorList = new List<Vector2Int>(_NodeA.NeighbouringTiles);
            for(int NodeBNeighIndex = 0; NodeBNeighIndex < _NodeB.NeighbouringTiles.Length; ++NodeBNeighIndex)
            {
                if (VectorList.Contains(_NodeB.NeighbouringTiles[NodeBNeighIndex]))
                {
                    Node CheckNode = NodeManager.Instance.ReturnNodeFromGridPos(_NodeB.NeighbouringTiles[NodeBNeighIndex]);
                    if(CheckNode == null)
                        HasSharedUnTreversableNode = true;
                    else if(CheckNode.IsOccupied == true)
                        HasSharedUnTreversableNode = true;
                }
            }
            return HasSharedUnTreversableNode;
        }


        #endregion
    }

    public class NodeHeap
    {
        Node[] NodesInHeap;
        int CurrentNodeCount;

        public NodeHeap(int _MaxSize)
        {
            NodesInHeap = new Node[_MaxSize];
        }

        //Adds Node to the heap and updates count
        public void Add(Node _Node)
        {
            _Node.HeapIndex = CurrentNodeCount;
            NodesInHeap[CurrentNodeCount] = _Node;
            SortUpNode(_Node);
            ++CurrentNodeCount;
        }

        //Removes first Node in heap, swaps it with the last Node in heap
        public Node RemoveFirstNodeOnHeap()
        {
            Node FirstNode = NodesInHeap[0];
            --CurrentNodeCount;
            NodesInHeap[0] = NodesInHeap[CurrentNodeCount];
            NodesInHeap[0].HeapIndex = 0;
            SortDownNode(NodesInHeap[0]);
            return FirstNode;
        }

        //Checks id Node is in heap
        public bool Contains(Node _Node)
        {
            return Equals(NodesInHeap[_Node.HeapIndex], _Node);
        }

        //Returns count of heap
        public int Count()
        {
            return CurrentNodeCount; 
        }

        //Moves up the priority of a Node publicly
        public void UpdateItem(Node _Node)
        {
            SortUpNode(_Node);
        }

        //Moves up the priority of a Node in the heap
        void SortUpNode(Node _Node)
        {
            int ParentNodeIndex = (_Node.HeapIndex - 1) / 2;
            while(true)
            {
                Node ParentItem = NodesInHeap[ParentNodeIndex];
                if (CompareNodes(_Node, ParentItem) > 0)
                {
                    SwapNodes(_Node, ParentItem);
                }
                else
                    break;
                ParentNodeIndex = (_Node.HeapIndex) / 2;
            }
        }

        //Moves Down the priority of a Node in the heap
        void SortDownNode(Node _Node)
        {
            while(true)
            {
                int ChildNodeIndexLeft = _Node.HeapIndex * 2 + 1;
                int ChildeNodeIndexRight = _Node.HeapIndex * 2 + 2;
                int SwapIndex = 0;
                if (ChildNodeIndexLeft < CurrentNodeCount)
                {
                    SwapIndex = ChildNodeIndexLeft;
                    if (ChildeNodeIndexRight < CurrentNodeCount)
                        if (CompareNodes(NodesInHeap[ChildNodeIndexLeft], NodesInHeap[ChildeNodeIndexRight]) < 0)
                            SwapIndex = ChildeNodeIndexRight;

                    if (CompareNodes(_Node, NodesInHeap[SwapIndex]) < 0)
                        SwapNodes(_Node, NodesInHeap[SwapIndex]);
                    else return;
                }
                else
                    return; 
            }
        }

        //Swaps the priority of two nodes in the heap (The Heap Index)
        void SwapNodes(Node _NodeA, Node _NodeB)
        {
            NodesInHeap[_NodeA.HeapIndex] = _NodeB;
            NodesInHeap[_NodeB.HeapIndex] = _NodeA;
            int NodeAIndex = _NodeA.HeapIndex;
            _NodeA.HeapIndex = _NodeB.HeapIndex;
            _NodeB.HeapIndex = NodeAIndex;
        }

        //Compare two nodes and gives outcome
        int CompareNodes(Node _StaticNode, Node _CompareToNode)
        {
            int CompareInt = CompareInts(_StaticNode.FCost , _CompareToNode.FCost);
            if(CompareInt == 0)
                CompareInt = CompareInts(_StaticNode.HCost, _CompareToNode.HCost);
            return -CompareInt;
        }

        int CompareInts(int _StaticInt, int _CompareInt)
        {

            if (_StaticInt > _CompareInt) //Indicate Higher Prioity 
                return 1;
            if (_StaticInt == _CompareInt) //Indicate equal Prioity 
                return 0;
            if (_StaticInt < _CompareInt) //Indicate Lower Prioity 
                return -1;
            return 0;
        }
    }
}
