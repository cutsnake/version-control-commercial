﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class EventManager : MonoBehaviour
    {
        public static event Action<Animal> EntityResister;
        public static event Action<Animal> EntityDeReg;
        public static event Action<InteractableObject> InteractiveObjectRegister;
        public static event Action<DigableObject> DiggableObjectRegister;

        static List<Animal> AnimalBackLog = new List<Animal>();
        static List<InteractableObject> IOBackLog = new List<InteractableObject>();
        static List<DigableObject> DOBackLog = new List<DigableObject>();

        //UI Events
        public static event Action<FlockData> FlockDataReg;
        public static event Action<bool> UIButtonSelected;
        public static event Action<SkillSelection> ReciveSelectedSkill;
        public static event Action ClearSkillSelection;

        //
        public static void InvokeEntityReg(Animal _An)
        {
            if(EntityResister == null)
            {
                AnimalBackLog.Add(_An);
                return;
            }
            if(EntityResister != null && AnimalBackLog.Count != 0)
            {
                for(int EntityIndex = 0; EntityIndex < AnimalBackLog.Count; ++EntityIndex)
                    EntityResister(AnimalBackLog[EntityIndex]);
                AnimalBackLog.Clear();
            }
            else
                EntityResister(_An);

        }
        public static void InvokeOjectReg(InteractableObject _IO)
        {
            if (InteractiveObjectRegister == null)
            {
                IOBackLog.Add(_IO);
                return;
            }
            if (InteractiveObjectRegister != null && IOBackLog.Count != 0)
            {
                for (int IOIndex = 0; IOIndex < IOBackLog.Count; ++IOIndex)
                {
                    InteractiveObjectRegister(IOBackLog[IOIndex]);
                    IOBackLog.Remove(IOBackLog[IOIndex]);
                }
            }
            else
                InteractiveObjectRegister(_IO);
        }

        public static void InvokeEnityDeReg(Animal _Animal)
        {
            if(EntityDeReg != null)
                EntityDeReg(_Animal);
        }
        
        
        public static void InvokeDigSiteReg(DigableObject _DO)
        {
            if (DiggableObjectRegister == null)
            {
                DOBackLog.Add(_DO);
                return;
            }
            if (DiggableObjectRegister != null && DOBackLog.Count != 0)
            {
                for (int DOIndex = 0; DOIndex < DOBackLog.Count; ++DOIndex)
                {
                    DiggableObjectRegister(DOBackLog[DOIndex]);
                    DOBackLog.Remove(DOBackLog[DOIndex]);
                }
            }
            else
                DiggableObjectRegister(_DO);
        }

        public static void InvokeButtonSelectedUpdate(bool _Toggle)
        {
            UIButtonSelected(_Toggle);
        }
        public static void InvokeSkillButtonSelection(SkillSelection _Selected)
        {
            ReciveSelectedSkill(_Selected);
        }
        public static void InvokeClearSkillSelection()
        {
            ClearSkillSelection();
        }

        public static void InvokeUISkillButtonUpdate(FlockData _Data)
        {
            FlockDataReg(_Data);
        }

        public static void NodeMangerSub(NodeManager _NM)
        {
            EntityResister += _NM.RegisterEntity;
            InteractiveObjectRegister += _NM.RegisterObject;
            DiggableObjectRegister += _NM.RegisterDigSite;

            InvokeEntityReg(null);
            InvokeOjectReg(null);
            InvokeDigSiteReg(null);
        }
    }
}
