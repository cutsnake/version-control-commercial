﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class PathFinderManager : MonoBehaviour
    {
        public static PathFinderManager Instance;

        public NodeManager NM;
        public Pathfinder[] PathFinders = new Pathfinder[2];
        public bool PathFinderIsActive = false;

        public Queue<PathRequest> PathRequests = new Queue<PathRequest>();
        public Queue<PathRequest> CompleatedRequests = new Queue<PathRequest>();

        public bool FrameToggle;

        public void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
        }

        private void Start()
        {
            NM = NodeManager.Instance;
            for (int PFIndex = 0; PFIndex < PathFinders.Length; ++PFIndex)
                PathFinders[PFIndex] = new Pathfinder();

        }

        private void Update()
        {
            if (!PathFinderIsActive)
                return;

            if (FrameToggle == false)
            {
                DesignateRequestsToPathfinder();
                FrameToggle = true;
            }
            else if (FrameToggle == true)
            {
                DistributePaths();
                FrameToggle = false;
            }
        }

        #region Public Utility Methods

        //This should be queued to be handled when ready to rather than a immediate method call
        public void RequestPathFromNodes(Node _StartingNode, Node _TargetNode, MovementCommand _Requestee)
        {
            if (!PathFinderIsActive)
                return;

            PathRequest PR = new PathRequest(_StartingNode, _TargetNode, _Requestee);
            PathRequests.Enqueue(PR);
        }

        //This should be queued to be handled when ready to rather than a immediate method call
        public void RequestRandomPathFromVec3(Vector3 _CurrentPos, MovementCommand _Requestee)
        {
            if (!PathFinderIsActive)
                return;

            Node StartingNode = NM.FindNodeFromWorldPosition(_CurrentPos);
            Node TargetNode = NM.GetRandNode();

            int RandAttempts = 0;

            if (TargetNode == null)
            {
                while (TargetNode == null)
                {
                    ++RandAttempts;
                    TargetNode = NM.GetRandNode();
                    if (RandAttempts > 20)
                        break;
                }
            }
            PathRequest PR = new PathRequest(StartingNode, TargetNode, _Requestee);
            PathRequests.Enqueue(PR);
        }

        //
        public void RequestPathFromVec3s(Vector3 _CurrentPos, Vector3 _TargetPos, MovementCommand _Requestee)
        {
            if (!PathFinderIsActive)
                return;

            Node StartingNode = NM.FindNodeFromWorldPosition(_CurrentPos);
            Node TargetNode = NM.FindNodeFromWorldPosition(_TargetPos);

            PathRequest PR = new PathRequest(StartingNode, TargetNode, _Requestee);
            PathRequests.Enqueue(PR);
        }

        #endregion

        #region Private Utility Methods

        //
        void DesignateRequestsToPathfinder()
        {
            if (PathRequests.Count == 0)
                return;
            for (int PFIndex = 0; PFIndex < PathFinders.Length; ++PFIndex)
            {
                if (PathFinders[PFIndex].CurrentStatus == PathfinderStatus.Incative)
                {
                    //Debug.Log("Path Request Sent to Pathfinder");
                    PathRequest PR = PathRequests.Dequeue();
                    PathFinders[PFIndex].SubmitFindPath(PR, this, NM);
                    break;
                }
            }
        }

        //If any requests have been compleated, returns their path
        void DistributePaths()
        {
            if (CompleatedRequests.Count == 0)
                return;

            for (int CRIndex = 0; CRIndex < CompleatedRequests.Count; ++CRIndex)
            {
                PathRequest CPR = CompleatedRequests.Dequeue();
                CPR.Requestee.RecivePathRequest(CPR);
            }
        }

        //Retrys failded path
        void RetryPath(PathRequest _CPR)
        {
            Debug.Log("Retrying Path");
            _CPR.IsBeingProcessed = false;
            _CPR.CompletedPath = null;

            PathRequests.Enqueue(_CPR);
        }

        //
        public bool IsPathToNodeClear(Node _Origin, Node _Target)
        {
            RaycastHit Hit;
            Vector3 OriginPos = _Origin.GetWorldPos();
            Vector3 TargetPos = _Target.GetWorldPos();
            Vector3 TargetDir = TargetPos - OriginPos;
            Ray CheckRay = new Ray(OriginPos, TargetDir);
            float DistanceToWaypoint = Vector3.Distance(TargetPos, OriginPos) + 0.25f;

            bool DidHit = Physics.Raycast(CheckRay, out Hit, DistanceToWaypoint);
            if (!DidHit)
                return true;

            return false;
        }
        #endregion
    }
}
