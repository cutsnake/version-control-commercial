﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
//using UnityEditor;

namespace AI
{
    public class NodeManager : MonoBehaviour
    {
        public static NodeManager Instance;
        public Node StaticNode;
        public bool IsReady = false;

        [Space]
        [Header("Grid Variables")]
        public int GridXLength;
        public int GridYLength;
        public float GridXScale;
        public float GridYScale;
        public Vector2 NodePositionGridScaleOffset;
        public Vector3 GridStartPos;
        public Collider FloorCollider;

        public Vector3 DestinationOffset = new Vector3(-0.5f, 0f, -0.5f);

        public Node[,] NodeGrid;

        [Space]
        [Header("Setup Variables")]
        public int FrameSkipValue = 0;
        public int FrameSkipRefresh = 5;
        public int NodeUpdateCurrentFrame = 5;

        Action<Animal> AddEntirtesAction;
        Action<Animal> RemoveEntitiesAction;

        [Space]
        [Header("Monitoring Lists")]
        public List<Animal> ActiveAnimals = new List<Animal>();
        public List<InteractableObject> ActiveInteractableObjects = new List<InteractableObject>();
        public List<DigableObject> ActiveDigableObjects = new List<DigableObject>();

        [Space]
        [Header("Debugging")]
        public bool VisualDebugging = false;
        public int DebugInt;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
        }

        private void Start()
        {
        }

        private void Update()
        {
            if (!IsReady)
                return;

            FrameMonitor(FrameSkipRefresh);
            MonitorAnimals();
        }

        #region Initial Setup Calls

        //Initiates and managers the creation of the node grid
        public void CallInitialSetup(Node[,] _NodeGrid, int _XLength, int _YLength, float _XScale, float _YScale, Vector2 _NodePosGridScaleOffset, Vector3 _GridPosStartPos, List<Animal> _ActiveAnimals)
        {
            NodeGrid = _NodeGrid;
            GridXLength = _XLength;
            GridYLength = _YLength;
            GridXScale = _XScale;
            GridYScale = _YScale;
            NodePositionGridScaleOffset = _NodePosGridScaleOffset;
            GridStartPos = _GridPosStartPos;

            Node.GridStartPos = GridStartPos;
            Node.NodePosGridScaleOffset = NodePositionGridScaleOffset;
            Node.TileSize = new Vector3(GridXScale, 0.1f, GridYScale);
            ActiveAnimals.AddRange(_ActiveAnimals);
            FloorCollider = gameObject.GetComponent<Collider>();

            PathFinderManager.Instance.NM = this;
            PathFinderManager.Instance.PathFinderIsActive = true;
            SubToEvents();
            IsReady = true;
        }

        //
        void SubToEvents()
        {
            EventManager.NodeMangerSub(this);
            EventManager.EntityDeReg += RemoveAnimal;
        }

        //Adds/Removes Animal classes to the created entity list
        public void RegisterEntity(Animal _Entity)
        {
            if (_Entity != null)
            {
                ActiveAnimals.Add(_Entity);
                FindNodeFromWorldPosition(_Entity.transform.position);
            }
        }

        public void RemoveAnimal(Animal _Aniaml)
        {
            if (ActiveAnimals.Contains(_Aniaml))
            {
                ActiveAnimals.Remove(_Aniaml);
                
            }
        }
        
        public void RegisterObject(InteractableObject _Object)
        {
            if (_Object != null)
            {
                ActiveInteractableObjects.Add(_Object);
                FindNodeFromWorldPosition(_Object.transform.position);
            }
        }
        public void RegisterDigSite(DigableObject _DiggableObject)
        {
            if (_DiggableObject != null)
            {
                ActiveDigableObjects.Add(_DiggableObject);
                FindNodeFromWorldPosition(_DiggableObject.transform.position);
            }
        }
        #endregion

        #region NodeGrid Setup



        #endregion

        #region Update Functions

        //Facilitate update method call stagger
        void FrameMonitor(int _FrameRotationLength)
        {
            if (FrameSkipValue >= _FrameRotationLength)
                FrameSkipValue = 0;
            else
                ++FrameSkipValue;
        }

        //Staggers Animal Update method calls
        void MonitorAnimals()
        {
            if (FrameSkipValue == NodeUpdateCurrentFrame)
                UpdateOccupiedNodes();
        }
        void MonitorOnjects()
        {
            if (FrameSkipValue == NodeUpdateCurrentFrame)
                UpdateOccupiedNodes();
        }

        //Grabs registered Animals positions, resets and assigns occupied nodes
        void UpdateOccupiedNodes()
        {
            if (!IsReady)
                return;
            SetEntityOccupiedNode();
            SetObjectOccupiedNodes();
        }

        void RemoveOccupancy(Node _LeavingNode)
        {
            _LeavingNode.Occupant = null;
            _LeavingNode.IsOccupied = false;
        }
        void SetOccupency(Node _EnteringNode, Entity _Ent)
        {
            _EnteringNode.Occupant = _Ent;
            _EnteringNode.IsOccupied = true;
        }
        void SetObjectOccupiedNodes()
        {
            Node RetrivedOccupiedNode = null;
            List<Node> NodesInColliderArea = new List<Node>();

            for (int ObjectIndex = 0; ObjectIndex < ActiveInteractableObjects.Count; ++ObjectIndex)
            {
                if (ActiveInteractableObjects[ObjectIndex] == null)
                    return;

                //Getting Occupied Node and NodesWithin area Rect
                RetrivedOccupiedNode = FindNodeFromWorldPosition(ActiveInteractableObjects[ObjectIndex].transform.position);
                if (RetrivedOccupiedNode == null)
                    return;

                //Remove Previous Occupant Nodes
                for (int PreviousAreaNodeIndex = 0; PreviousAreaNodeIndex < ActiveInteractableObjects[ObjectIndex].OccupyingNodes.Length; ++PreviousAreaNodeIndex)
                {
                    if (ActiveInteractableObjects[ObjectIndex].OccupyingNodes[PreviousAreaNodeIndex] != null)
                        RemoveOccupancy(ActiveInteractableObjects[ObjectIndex].OccupyingNodes[PreviousAreaNodeIndex]);
                }
                ActiveInteractableObjects[ObjectIndex].OccupyingNodes = null;

                //Grabs New Area Nodes
                if (ActiveInteractableObjects[ObjectIndex].ThisCollider.bounds.extents != null)
                {
                    ActiveInteractableObjects[ObjectIndex].OccupyingNodes = GetNodesInRectArea(ActiveInteractableObjects[ObjectIndex].ThisCollider.bounds.extents, RetrivedOccupiedNode).ToArray();
                    NodesInColliderArea.AddRange(ActiveInteractableObjects[ObjectIndex].OccupyingNodes);
                }

              
                //Cycle Through New Area Nodes
                for (int AreaNodeIndex = 0; AreaNodeIndex < NodesInColliderArea.Count; ++AreaNodeIndex)
                {
                    if (NodesInColliderArea[AreaNodeIndex] != null && RetrivedOccupiedNode != null)
                    {
                        RemoveOccupancy(NodesInColliderArea[AreaNodeIndex]);
                        ActiveInteractableObjects[ObjectIndex].OccupiedNode = RetrivedOccupiedNode;
                        SetOccupency(NodesInColliderArea[AreaNodeIndex], ActiveInteractableObjects[ObjectIndex]);
                    }
                }
                NodesInColliderArea.Clear();
            }
        }
        void SetEntityOccupiedNode()
        {
            Node RetrivedOccupiedNode = null;
            for (int AnimalIndex = 0; AnimalIndex < ActiveAnimals.Count; ++AnimalIndex)
            {
                if (ActiveAnimals[AnimalIndex] == null)
                    continue;
                RetrivedOccupiedNode = FindNodeFromWorldPosition(ActiveAnimals[AnimalIndex].transform.position);
                if (ActiveAnimals[AnimalIndex].OccupiedNode != null && RetrivedOccupiedNode != null)
                {
                    RemoveOccupancy(ActiveAnimals[AnimalIndex].OccupiedNode);
                    ActiveAnimals[AnimalIndex].OccupiedNode = RetrivedOccupiedNode;
                    SetOccupency(RetrivedOccupiedNode, ActiveAnimals[AnimalIndex]);
                }
            }
        }

        public void CheckSetDigNodes()
        {
            for (int DigSiteIndex = 0; DigSiteIndex < ActiveDigableObjects.Count; DigSiteIndex++)
            {
                if(!ActiveDigableObjects[DigSiteIndex].NodesSet)
                    ActiveDigableObjects[DigSiteIndex].SetAllNodes();
            }
        }


        #endregion

        #region Utility Methods

        //Finds a node based on world pos
        public Node FindNodeFromWorldPosition(Vector3 _WorldPos)
        {
            if (!IsReady)
                return null;
            _WorldPos = _WorldPos - GridStartPos + DestinationOffset;

            float XIndex = _WorldPos.x * GridXScale;
            float YIndex = _WorldPos.z * GridYScale;

            int XIntIndex = Mathf.RoundToInt(XIndex);
            int YIntIndex = Mathf.RoundToInt(YIndex);

            if (XIntIndex > GridXLength -1 || YIntIndex > GridYLength -1 || XIntIndex < 0 || YIntIndex < 0)
            {
                return null;
            }
            Node ReturnNode = NodeGrid[XIntIndex, YIntIndex];
            if (ReturnNode != null)
            {
                return ReturnNode;
            }
            else
                return null;
        }

        //Returns a Random node from Node Grid, tries 10 times. (This could be improved with a check against a null node list..)
        public Node GetRandNode()
        {
            if (!IsReady)
                return null;

            Node RandNode = null;
            int Attempts = 0;

            while(RandNode == null || Attempts < 10)
            {
                int Ranx = UnityEngine.Random.Range(0, GridXLength);
                int RanY = UnityEngine.Random.Range(0, GridYLength);
                Vector3 RandVec = new Vector3(Ranx, 0, RanY);
                RandNode = FindNodeFromWorldPosition(RandVec);
            }
            return RandNode;
        }

        public Node ReturnNodeFromGridPos(Vector2Int _Pos)
        {
            return NodeGrid[_Pos.x, _Pos.y];
        }

        //Nest for loop iteration between grid positions to obtain nodes in a Bounds area
        public List<Node> GetNodesInRectArea(Vector3 _Extents, Node _BoundsCentre)
        {
            List<Node> NodeList = new List<Node>();
            Node TopLeft = FindNodeFromWorldPosition(_BoundsCentre.GetWorldPos() + new Vector3(-_Extents.x, 0, _Extents.z));
            Node BotRight = FindNodeFromWorldPosition(_BoundsCentre.GetWorldPos() + new Vector3(_Extents.x, 0, -_Extents.z));

            if (TopLeft == null || BotRight == null)
                return NodeList;

            int XMin = TopLeft.GridPosition.x;
            int XMax = BotRight.GridPosition.x;
            int YMin = TopLeft.GridPosition.y;
            int YMax = BotRight.GridPosition.y;

            for (int XINdex = TopLeft.GridPosition.x; XINdex < BotRight.GridPosition.x + 1; ++XINdex)
                for(int YINdex = TopLeft.GridPosition.y; YINdex > BotRight.GridPosition.y - 1; --YINdex)
                    NodeList.Add(NodeGrid[XINdex, YINdex]);
            return NodeList;
        }

        public DigableObject ReturnNodeDigObject(Node _SelectedNode)
        {
            CheckSetDigNodes();

            for(int DigIndex = 0; DigIndex < ActiveDigableObjects.Count; ++DigIndex)
            {
                ActiveDigableObjects[DigIndex].SetAllNodes();
                if (ActiveDigableObjects[DigIndex].CurrentDigNode.GridPosition == _SelectedNode.GridPosition)
                    return ActiveDigableObjects[DigIndex];
            }
            return null;
        }
        #endregion
    }
}
