﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

namespace AI
{
    public class NodeManagerSetup : MonoBehaviour
    {
        public NodeManager NM;
        public Node StaticNode;

        [Header("Input Variables")]
        public string MapName = "Map";
        public Vector2 NodeRatio = new Vector2(1, 1);
        public float NodeElevationFromCollider = 0.3f;

        [Space]
        [Header("Node/Obsticale Removal Variables")]
        public float ObsticleOverlapPercentageAllowance = 0.1f;

        [Space]
        [Header("Grid Setup")]
        public GridCreationMethod SelectedMethod = GridCreationMethod.GridSizeIndependent;

        [Space]
        [Header("Floor Variables")]
        public float FloorXLength; 
        public float FloorYLength; 
        public float FloorElevation;
        public Collider FloorCollider;

        [Space]
        [Header("Node Variables")]
        public Vector3 GridStartPos;
        public float NodeHight = 0.1f;

        [Space]
        [Header("Grid Variables")]
        public int GridXLength;
        public int GridYLength;
        public float GridXScale;
        public float GridYScale;
        public Vector2 NodePositionGridScaleOffset;

        public Node[,] NodeGrid;
        [SerializeField]
        public NodeDataOutput NDO;
        public List<Animal> ActiveAnimals = new List<Animal>();
        public List<InteractableObject> InteractableObjects = new List<InteractableObject>();

        [Space]
        [Header("Setup Status")]
        public Thread NodeSetupThread;
        public ProgressState CurrentGridState = ProgressState.Inactive;
        public ProgressState CurrentTileRemovalState = ProgressState.Inactive;
        public ProgressState CurrentNeighbourState = ProgressState.Inactive;
        public ProgressState CurrentConvertState = ProgressState.Inactive;

        [Space]
        [Header("Setup Variables")]
        public float SetupTotalTime = -1;
        public int RemoveNonFloorTileInterval = 50;
        public bool SetupCompletate;
        public bool NodeDataSaved;

        [Space]
        [Header("NodeRemoval/Relocaton Variables")]
        public float FloorCheckDistance;
        public float CheckOverLap;
        public float CombinedCheckDistance;

        [Space]
        [Header("Debugging")]
        public bool VisualDebugging = false;
        public int DebugInt;

        //
        private void Start()
        {
            //NDO = GetComponent<NodeDataOutput>();
            StartCoroutine(GameStartInitialSetup());
        }

        #region Initial Setup Calls

        //Initiates and managers the creation of the node grid
        public void CallInitialSetup()
        {
            if (CurrentGridState != ProgressState.Complete && NodeGrid == null)
                StartCoroutine(InitialSetup());
        }
        IEnumerator InitialSetup()
        {
            GetFloorGridDimentions();
            CallCreateGrid();

            yield return new WaitUntil(() => CurrentGridState == ProgressState.Complete);
            CallTileRemoval();

            yield return new WaitUntil(() => CurrentTileRemovalState == ProgressState.Complete);
            NeighbourSetThreadCall();

            yield return new WaitUntil(() => CurrentNeighbourState == ProgressState.Complete);
            SetupCompletate = true;
            //PathFinderManager.Instance.PathFinderIsActive = true;
        }

        //
        IEnumerator GameStartInitialSetup()
        {
            NM = this.gameObject.AddComponent<NodeManager>();
            Node[,] PassNodes = NDO.RequestDataOutput();
            NM.CallInitialSetup(PassNodes, GridXLength, GridYLength, GridXScale, GridYScale, NodePositionGridScaleOffset, GridStartPos, ActiveAnimals);
            yield return new WaitUntil(() => NDO.OutputState == ProgressState.Complete);
            NDO.CleanUp();

            Destroy(this);
        }

        #endregion

        #region NodeGrid Setup

        //Grabs the attached collider and sets values based on dimentions
        public void GetFloorGridDimentions()
        {
            StaticNode = new Node();

            FloorCollider = this.gameObject.GetComponent<Collider>();
            Bounds ColBounds = FloorCollider.bounds;
            FloorXLength = ColBounds.size.x;
            FloorYLength = ColBounds.size.z;

            FloorElevation = ColBounds.size.y;
            FloorCheckDistance = ColBounds.size.y;

            GridStartPos = new Vector3((ColBounds.center.x - ColBounds.extents.x), (FloorElevation + NodeElevationFromCollider), (ColBounds.center.z - ColBounds.extents.z));

            GridXScale = NodeRatio.x;
            GridYScale = NodeRatio.y;
            NodePositionGridScaleOffset = new UnityEngine.Vector2((GridXScale / 2), (GridYScale / 2));
            CheckOverLap = 1f - ObsticleOverlapPercentageAllowance;

            CombinedCheckDistance = FloorCheckDistance + NodeElevationFromCollider + 0.2f;

            switch (SelectedMethod)
            {
                case GridCreationMethod.GridSizeDependent:
                    break;
                case GridCreationMethod.GridSizeIndependent:
                    GridXLength = (int)(FloorXLength / GridXScale);
                    GridYLength = (int)(FloorYLength / GridYScale);
                    break;
            }
        }

        //Assigns the Node creation method to a seperate thread and calls it.
        public void CallCreateGrid()
        {
            if (NodeGrid != null || CurrentGridState != ProgressState.Inactive)
                return;
            CurrentGridState = ProgressState.InProgress;
            CurrentGridState = ProgressState.InProgress;
            Thread GridCreationThread = new Thread(CreateGrindThreadCall);
            GridCreationThread.Start();
        }

        //Iterates through a nested for loop and creates and sets a Node for each space in the 2D array
        void CreateGrindThreadCall()
        {
            Node.TileSize = new Vector3(GridXScale, 0.1f, GridYScale);
            Node.GridStartPos = GridStartPos;
            Node.NodePosGridScaleOffset = NodePositionGridScaleOffset;

            Node[,] NewTileGrid = new Node[GridXLength, GridYLength];
            for (int XIndex = 0; XIndex < GridXLength; ++XIndex)
            {
                for (int YIndex = 0; YIndex < GridYLength; ++YIndex)
                {
                    Node NewNodeScript = new Node();
                    NewNodeScript.GridPosition = new Vector2Int(XIndex, YIndex);
                    NewTileGrid[XIndex, YIndex] = NewNodeScript;
                }
            }
            NodeGrid = NewTileGrid;
            CurrentGridState = ProgressState.Complete;
        }

        //Calls the Removal of uneeded nodes (Off grid and obstructed)
        public void CallTileRemoval()
        {
            if (NodeGrid == null || CurrentGridState != ProgressState.Complete)
                return;
            CurrentTileRemovalState = ProgressState.InProgress;
            CurrentTileRemovalState = ProgressState.InProgress;
            RemoveNonFloorTiles();
        }

        //Iterates through Node array calling Raycasts to detect walkable surface, then checks for obstructions.
        public void RemoveNonFloorTiles()
        {
            RaycastHit LocalHit;
            //Vector3 HalfExents = (new Vector3(GridXScale - 0.01f, 0.1f, GridYScale - 0.01f) * CheckOverLap) / 2;
            for (int XIndex = 0; XIndex < GridXLength; ++XIndex)
            {
                for (int YIndex = 0; YIndex < GridYLength; ++YIndex)
                {
                    if (Physics.Raycast(NodeGrid[XIndex, YIndex].GetWorldPos(), transform.TransformDirection(Vector3.down), out LocalHit, CombinedCheckDistance))
                    {
                        switch (ColliderOverlapCheck(NodeGrid[XIndex, YIndex], FloorCollider))
                        {
                            case ColliderOwnerType.Null:

                                break;
                            case ColliderOwnerType.AI:
                                if(!ActiveAnimals.Contains((Animal)NodeGrid[XIndex, YIndex].Occupant))
                                    ActiveAnimals.Add((Animal)NodeGrid[XIndex, YIndex].Occupant);
                                break;
                            case ColliderOwnerType.InteractiveObject:
                                if (!InteractableObjects.Contains((InteractableObject)NodeGrid[XIndex, YIndex].Occupant))
                                    InteractableObjects.Add((InteractableObject)NodeGrid[XIndex, YIndex].Occupant);
                                break;
                            case ColliderOwnerType.Object:
                                NodeGrid[XIndex, YIndex] = null;
                                break;
                        }
                    }
                    else
                        NodeGrid[XIndex, YIndex] = null;
                }
            }
            CurrentTileRemovalState = ProgressState.Complete;
        }

        #endregion

        #region NeighbourSetup

        //Assigns Neighbouring tile setup to alternative thread and calls method.
        public void NeighbourSetThreadCall()
        {
            if (CurrentGridState != ProgressState.Complete)
                return;

            CurrentNeighbourState = ProgressState.InProgress;
            NodeSetupThread = new Thread(SetTileNeighbours);
            NodeSetupThread.Start();
        }

        //Iterates through Node array to call neightbour setup, skips nulls
        void SetTileNeighbours()
        {
            if (NodeGrid == null)
            {
                CurrentNeighbourState = ProgressState.Inactive;
                return;
            }

            for (int XIndex = 0; XIndex < GridXLength; ++XIndex)
                for (int YIndex = 0; YIndex < GridYLength; ++YIndex)
                    if (NodeGrid[XIndex, YIndex] != null)
                    {
                        SetNeighbouringNodeTiles(NodeGrid[XIndex, YIndex]);
                    }
            CurrentNeighbourState = ProgressState.Complete;
        }

        //Iterates through neighbouring tiles using Grid, validates heighbour existance and calls cornering node checks.
        public void SetNeighbouringNodeTiles(Node _NodeTile)
        {
            if (_NodeTile == null)
                return;

            List<Node> Neighbours = new List<Node>();
            //bool[] IsCornerTurn = new bool[8];
            //int NeighbourIndex = 0;

            for (int XIndex = -1; XIndex <= 1; XIndex++)
            {
                for (int ZIndex = -1; ZIndex <= 1; ZIndex++)
                {
                    if (XIndex == 0 && ZIndex == 0)
                    {
                        continue;
                    }

                    int CheckX = (int)_NodeTile.GridPosition.x + XIndex;
                    int CheckZ = (int)_NodeTile.GridPosition.y + ZIndex;

                    if (CheckX >= 0 && CheckX < GridXLength && CheckZ >= 0 && CheckZ < GridYLength)
                    {
                        if (NodeGrid[CheckX, CheckZ] != null)
                        {
                            Neighbours.Add(NodeGrid[CheckX, CheckZ]);
                        }
                        //else
                        //{
                        //    Neighbours[NeighbourIndex] = null;
                        //}
                        //++NeighbourIndex;
                    }
                }
            }
            //IsCornerTurn = DetermineCorners(Neighbours);
            _NodeTile.SetNeighbours(Neighbours);
            //_NodeTile.ConnectionCorners = IsCornerTurn;

        }

        ////Determines corner nodes from neighbour occupation
        //bool[] DetermineCorners(Node[] _Neighbours)
        //{
        //    bool[] Corners = new bool[_Neighbours.Length];

        //    if (_Neighbours[0] != null)
        //        if (_Neighbours[1] == null || _Neighbours[3] == null)
        //            Corners[0] = true;

        //    if (_Neighbours[2] != null)
        //        if (_Neighbours[1] == null || _Neighbours[4] == null)
        //            Corners[2] = true;

        //    if (_Neighbours[5] != null)
        //        if (_Neighbours[6] == null || _Neighbours[3] == null)
        //            Corners[5] = true;

        //    if (_Neighbours[7] != null)
        //        if (_Neighbours[6] == null || _Neighbours[4] == null)
        //            Corners[7] = true;
        //    return Corners;
        //}
        #endregion

        #region Save Map Data Methods

        //Calls Save Grid Data
        public void CallSaveGridMap()
        {
            if (CurrentConvertState != ProgressState.Inactive)
                return;
            CurrentConvertState = ProgressState.InProgress;
            StartCoroutine(SaveGridMap());
        }
        //Requests the NDO to save grid Information
        IEnumerator SaveGridMap()
        {
            if (NDO == null)
            {
                NDO = gameObject.AddComponent<NodeDataOutput>();
            }
            NDO.RequestDataInput(NodeGrid, GridXLength, GridYLength, MapName);

            yield return new WaitUntil(() => NDO.InputState == ProgressState.Complete);

            NodeDataSaved = true;
            NodeGrid = null;
            //NDO.CleanUp();
            CurrentConvertState = ProgressState.Complete;
        }
        #endregion

        #region Visual Debugging

        //Gizmo Visual Debugging (Remember to turn off in game)
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            if(NodeDataSaved)
                Gizmos.color = Color.green;
            Gizmos.DrawWireCube(transform.position + new Vector3(0, NodeElevationFromCollider + FloorElevation, 0), new Vector3(FloorXLength, NodeHight, FloorYLength));

            if (NodeGrid != null && CurrentGridState == ProgressState.Complete)
            {
                if (VisualDebugging)
                {
                    for (int XIndex = 0; XIndex < GridXLength; ++XIndex)
                        for (int YIndex = 0; YIndex < GridYLength; ++YIndex)
                        {
                            if (NodeGrid[XIndex, YIndex] != null)
                            {
                                Gizmos.DrawWireCube((NodeGrid[XIndex, YIndex].GetWorldPos()), Node.TileSize);
                            }
                        }
                }
            }
        }
        #endregion

        #region Utility Methods

        //Checks if all segments are compleated and ready
        bool CheckForSetup()
        {
            bool Ready = true;
            if (SetupCompletate == false)
                Ready = false;
            if (CurrentGridState == ProgressState.Inactive)
                Ready = false;
            if (CurrentTileRemovalState == ProgressState.Inactive)
                Ready = false;
            if (CurrentNeighbourState == ProgressState.Inactive)
                Ready = false;

            return Ready;
        }

        //Resets the values of the NodeManager
        public void ResetGrid()
        {
            if (NodeSetupThread != null)
                NodeSetupThread.Abort();
            NodeGrid = null;
            DestroyImmediate(NDO);
            NDO = null;
            CurrentGridState = ProgressState.Inactive;
            CurrentNeighbourState = ProgressState.Inactive;
            CurrentTileRemovalState = ProgressState.Inactive;
            CurrentConvertState = ProgressState.Inactive;
            FloorXLength = 0;
            FloorYLength = 0;
            FloorElevation = 0;
            FloorCollider = null;
            NodeHight = 0;
            GridXLength = 0;
            GridYLength = 0;
            GridXScale = 0;
            GridYScale = 0;
            FloorCheckDistance = 0;
            CheckOverLap = 0;
            CombinedCheckDistance = 0;
            GridStartPos = Vector3.zero;
            ActiveAnimals.Clear();
            InteractableObjects.Clear();
            SetupCompletate = false;
            NodeDataSaved = false;
        }

        //Undertakes a physics check to determine obstructions using physics.overlapbox to detect colliders.
        public ColliderOwnerType ColliderOverlapCheck(Node _Node, Collider _FloorCol)
        {
            Collider[] NC = Physics.OverlapBox(_Node.GetWorldPos(), Node.TileSize / 3);

            if (NC == null)
                return ColliderOwnerType.Null;

            for (int index = 0; index < NC.Length; ++index)
            {
                if (NC[index] == _FloorCol || NC[index].isTrigger == true)
                    continue;

                Animal Anim = NC[0].gameObject.GetComponent<Animal>();
                InteractableObject InteractableObject = NC[0].gameObject.GetComponent<InteractableObject>();

                if (Anim == null)
                    Anim = NC[0].gameObject.GetComponentInParent<Animal>();
                if(InteractableObject == null)
                    InteractableObject = NC[0].gameObject.GetComponentInParent<InteractableObject>();

                if (Anim != null)
                {
                    _Node.Occupant = Anim;
                    _Node.IsOccupied = true;
                    return ColliderOwnerType.AI;
                }
                else if(InteractableObject != null)
                {
                    _Node.Occupant = InteractableObject;
                    _Node.IsOccupied = true;
                    return ColliderOwnerType.InteractiveObject;
                }
                else
                    return ColliderOwnerType.Object;
            }
            return ColliderOwnerType.Null;
        }

        #endregion
    }
}