﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    [Serializable]
    public class Node 
    {
        public Vector2Int GridPosition;

        public Vector2Int[] NeighbouringTiles = null;
        public bool IsOccupied;
        public Entity Occupant;

        public int GCost;
        public int HCost;
        public int FCost { get { return (GCost + HCost); } }
        public int HeapIndex;
        [SerializeField]
        public Node ParentNode;

        //Static Variables (Memory Saving)
        public static Vector3 TileSize;
        public static Vector3 NodePosGridScaleOffset;
        public static Vector3 GridStartPos;

        //Returns World Pos based on GridPosition (Memory saving)
        public Vector3 GetWorldPos()
        {
            Vector3 Pos = new Vector3(((GridPosition.x * TileSize.x) + NodePosGridScaleOffset.x), TileSize.y, ((GridPosition.y * TileSize.z) + NodePosGridScaleOffset.y)) + GridStartPos;
            return Pos;
        }

        //Returns Neighbouring Tiles after converstion
        public Node[] GetNeighbouringNodes()
        {
            int Size = NeighbouringTiles.Length;
            //Vector2 IgnoreVec = new Vector2(999, 999);
            Node[] NodeNeighbours = new Node[Size];
            for (int NeihIndex = 0; NeihIndex < NeighbouringTiles.Length; ++NeihIndex)
            {
                int X = NeighbouringTiles[NeihIndex].x;
                int Y = NeighbouringTiles[NeihIndex].y;
                NodeNeighbours[NeihIndex] = NodeManager.Instance.NodeGrid[X, Y];
            }
            return NodeNeighbours;
        }

        //Convert Node[] into GridPos Array
        public void SetNeighbours(List<Node> _Neighbours)
        {
            //int Size = _Neighbours.Count;
            Vector2Int[] NeighBourGridPos = new Vector2Int[_Neighbours.Count];
            for (int NeihIndex = 0; NeihIndex < _Neighbours.Count; ++NeihIndex)
            {
                //if (_Neighbours[NeihIndex] == null)
                //{
                //    NeighBourGridPos[NeihIndex] = new Vector2Int(999, 999);
                //    continue;
                //}
                NeighBourGridPos[NeihIndex] = _Neighbours[NeihIndex].GridPosition;
            }
            NeighbouringTiles = NeighBourGridPos;
        }
    }
}
