﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    //Manager Enums
    public enum GridCreationMethod
    {
        GridSizeDependent,
        GridSizeIndependent
    }

    public enum ProgressState
    {
        Inactive,
        InProgress,
        Complete
    }

    public enum ColliderOwnerType
    {
        Null,
        AI,
        InteractiveObject,
        Object
    }

    //PathFinder Enums
    public enum PathfinderStatus
    {
        Incative,
        Active
    }

    public enum MovementStatus
    {
        Null,
        Idle,
        MovingToNextWapoint,
        ArrivedAtWaypoint,
        ArrivedAtTargetNode
    }

    public enum PathRequestStatus
    {
        NoneRequested,
        RequestedAndWaiting,
        RecivedAndValid
    }

    public enum WaypointStatus
    {
        AtWaypoint,
        AtTargetNode,
        BetweenWaypoints
    }

    //
    public enum AnimalState
    {
        Idle,
        Wondering,
        InFlock,
        Moving,
        UndertakingTask,
        Fleeing
    }



    //UI Enums
    public enum CameraInputState
    {
        ButtonInput,
        KeyboardInput
    }

    public enum MovementDirection
    {
        North,
        South,
        East,
        West,
        Null
    }

    public enum SkillSelection
    {
        Null,
        Push,
        RemoveLight,
        RemoveHeavy,
        Dig
    }

    public enum FollowState
    {
        FollowingAnimal,
        ReturningToAnimal,
        IndependentMovement
    }

    public enum RotationDirection
    {
        Clockwise,
        AntiClockwise,
        Null
    }

    //Command Enums
    public enum InteractionCommandState
    {
        Idle,
        MovingToPosition,
        FacingTarget,
        UndertakingAction,
        ActionFinished
    }

    public enum FlockingState
    {
        Idle,
        Wonder,
        Seeking,
        Avoiding
    }

    public enum DigDirection
    {
        StartToEnd,
        EndToStart
    }

    //Unit Enums
    public enum AnimalTypes
    {
        Dog,
        Fox,
        Cow,
        Chicken,
        Pig,
        Sheep,
        Null
    }

    public enum AnimalStatus
    {
        Idle,
        Wondering,
        Moving,
        Flocking,
        UndertakingAciton,
    }
}
