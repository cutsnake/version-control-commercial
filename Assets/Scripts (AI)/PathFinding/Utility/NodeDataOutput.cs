﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace AI
{
    [System.Serializable]
    public class NodeDataOutput : MonoBehaviour
    {
        public string MapName;
        public int XLength;
        public int YLength;
        public int NodeCount;

        [HideInInspector]
        [SerializeField]
        public List<Node> DataList = new List<Node>();
        private Node[,] NodeGrid;

        public ProgressState InputState = ProgressState.Inactive;
        public ProgressState OutputState = ProgressState.Inactive;

        public void RequestDataInput(Node[,] _NodeGrid, int _XLength, int _YLength, string _MapName)
        {
            InputState = ProgressState.InProgress;
            NodeGrid = _NodeGrid;
            XLength = _XLength;
            YLength = _YLength;
            MapName = _MapName;

            Thread GridCreationThread = new Thread(InputData);
            GridCreationThread.Start();
        }
        void InputData()
        {
            DataList.Clear();
            Debug.Log(DataList.Count);
            for (int XIndex = 0; XIndex < XLength; ++XIndex)
            {
                for (int YIndex = 0; YIndex < YLength; ++YIndex)
                {
                    if (NodeGrid[XIndex, YIndex] == null)
                        continue;

                    DataList.Add(NodeGrid[XIndex, YIndex]);
                    ++NodeCount;
                }
            }
            NodeGrid = null;
            NodeCount = DataList.Count;
            InputState = ProgressState.Complete;
        }

        //
        public Node[,] RequestDataOutput()
        {
            OutputState = ProgressState.InProgress;

            Thread GridCreationThread = new Thread(OutputData);
            GridCreationThread.Start();

            while (OutputState == ProgressState.InProgress)
            {

            }
            DataList.Clear();
            return NodeGrid;
        }
        void OutputData()
        {
            NodeGrid = new Node[XLength, YLength];
            int Gridx;
            int Gridy;
            for (int ListIndex = 0; ListIndex < DataList.Count; ++ListIndex)
            {
                Gridx = DataList[ListIndex].GridPosition.x;
                Gridy = DataList[ListIndex].GridPosition.y;
                NodeGrid[Gridx, Gridy] = DataList[ListIndex];
            }
            OutputState = ProgressState.Complete;
        }
        public void CleanUp()
        {
            DataList.Clear();
            NodeGrid = null;
        }
    }
}
