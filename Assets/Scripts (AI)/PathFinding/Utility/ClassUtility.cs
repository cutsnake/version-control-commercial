﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    //Hold Path request information to use later
    public class PathRequest
    {
        public PathRequest(Node _StartingNode, Node _TargetNode, MovementCommand _Requestee)
        {
            StartingNode = _StartingNode;
            TargetNode = _TargetNode;
            Requestee = _Requestee;
            PathIsFound = false;
            IsBeingProcessed = false;
            CompletedPath = null;
        }

        public bool PathIsFound;
        public bool IsBeingProcessed;
        public Node StartingNode;
        public Node TargetNode;
        public MovementCommand Requestee;
        public List<Node> CompletedPath;
    }

    //
    public class NodePath
    {
        public List<Node> Waypoints;
        public PathSmoothingLine[] TurningBoundries;
        public int FinishLineIndex;

        public NodePath(List<Node> _Waypoints, Node _StartPos, float _TurnDistance)
        {
            Waypoints = _Waypoints;
            TurningBoundries = new PathSmoothingLine[Waypoints.Count];
            FinishLineIndex = TurningBoundries.Length - 1;

            Vector3 StartPos = _StartPos.GetWorldPos();
            Vector2 PreviousPoint = new Vector2(StartPos.x, StartPos.z);
            for(int NodeIndex = 0; NodeIndex < Waypoints.Count; ++NodeIndex)
            {
                Vector3 CurrentNodeWorldPos = Waypoints[NodeIndex].GetWorldPos();
                Vector2 CurrentPoint = new Vector2(CurrentNodeWorldPos.x, CurrentNodeWorldPos.z);
                Vector2 DirectionToCurrentPoint = (CurrentPoint - PreviousPoint).normalized;

                Vector2 TurnBoundryPoint = (NodeIndex == FinishLineIndex) ? CurrentPoint : CurrentPoint - DirectionToCurrentPoint * _TurnDistance;
                TurningBoundries[NodeIndex] = new PathSmoothingLine(TurnBoundryPoint, PreviousPoint - DirectionToCurrentPoint * _TurnDistance);
                PreviousPoint = TurnBoundryPoint;

            }
        }
    }

    public class FlockData
    {
        public int CowsInFlock;
        public int ChickensInFlock;
        public int PigsInFlock;
        public int SheepInFlock;
        public DogFlock Flock;
    }

    //public class NodeDataOutput : MonoBehaviour
    //{
    //    public string MapName;
    //    public int XLength;
    //    public int YLength;
    //    public int NodeCount;

    //    [HideInInspector]
    //    [SerializeField]
    //    public List<Node> DataList = new List<Node>();
    //    private Node[,] NodeGrid;

    //    public ProgressState InputState = ProgressState.Inactive;
    //    public ProgressState OutputState = ProgressState.Inactive;

    //    public void RequestDataInput(Node[,] _NodeGrid, int _XLength, int _YLength, string _MapName)
    //    {
    //        InputState = ProgressState.InProgress;
    //        NodeGrid = _NodeGrid;
    //        XLength = _XLength;
    //        YLength = _YLength;
    //        MapName = _MapName;

    //        Thread GridCreationThread = new Thread(InputData);
    //        GridCreationThread.Start();
    //    }
    //    void InputData()
    //    {
    //        DataList.Clear();
    //        for (int XIndex = 0; XIndex < XLength; ++XIndex)
    //        {
    //            for (int YIndex = 0; YIndex < YLength; ++YIndex)
    //            {
    //                if (NodeGrid[XIndex, YIndex] == null)
    //                    continue;

    //                DataList.Add(NodeGrid[XIndex, YIndex]);
    //                ++NodeCount;
    //            }
    //        }
    //        NodeGrid = null;
    //        InputState = ProgressState.Complete;
    //    }

    //    //
    //    public Node[,] RequestDataOutput()
    //    {
    //        OutputState = ProgressState.InProgress;

    //        Thread GridCreationThread = new Thread(OutputData);
    //        GridCreationThread.Start();

    //        while (OutputState == ProgressState.InProgress)
    //        {

    //        }
    //        DataList.Clear();
    //        return NodeGrid;
    //    }
    //    void OutputData()
    //    {
    //        NodeGrid = new Node[XLength, YLength];
    //        int Gridx;
    //        int Gridy;
    //        for (int ListIndex = 0; ListIndex < DataList.Count; ++ListIndex)
    //        {
    //            Gridx = DataList[ListIndex].GridPosition.x;
    //            Gridy = DataList[ListIndex].GridPosition.y;
    //            NodeGrid[Gridx, Gridy] = DataList[ListIndex];
    //        }
    //        OutputState = ProgressState.Complete;
    //    }
    //    public void CleanUp()
    //    {
    //        DataList.Clear();
    //        NodeGrid = null;
    //    }
    //}

    public struct PathSmoothingLine
    {
        const float VertinalLineGradient = 1e5f;

        float Gradient;
        float YIntercept;
        float GradentOfPerpendicular;
        Vector2 PointOnLine1;
        Vector2 PointOnLine2;

        bool ApproachSide;

        public PathSmoothingLine(Vector2 _PointOnLine, Vector2 _PointPerpendicular)
        {
            Vector2 _PP = _PointPerpendicular;
            float DirectionX = _PointOnLine.x - _PointPerpendicular.x;
            float DirectionY = _PointOnLine.y - _PointPerpendicular.y;

            if (DirectionX == 0)
                GradentOfPerpendicular = VertinalLineGradient;
            else
                GradentOfPerpendicular = DirectionY / DirectionX;

            if (GradentOfPerpendicular == 0)
                Gradient = VertinalLineGradient;
            else
                Gradient = -1 / GradentOfPerpendicular;

            YIntercept = _PointOnLine.y - Gradient * _PointOnLine.x;
            PointOnLine1 = _PointOnLine;
            PointOnLine2 = _PointOnLine + new Vector2(1, Gradient);

            ApproachSide = false;
            ApproachSide = GetSide(_PP);
        }

        bool GetSide(Vector2 _Point)
        {
            float CompareSideA = (_Point.x - PointOnLine1.x) * (PointOnLine2.y - PointOnLine1.y);
            float CompareSideB = (_Point.y - PointOnLine1.y) * (PointOnLine2.x - PointOnLine1.x);
            return CompareSideA > CompareSideB;
        }

        public bool HasCrossedLine(Vector2 _Point)
        {
            return GetSide(_Point) != ApproachSide;
        }
    }

}
