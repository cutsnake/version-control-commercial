﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class DogFlock : MonoBehaviour {

        public Animal ThisDog;
        public List<Animal> AnimalsInFlock = new List<Animal>();
        public List<FlockCommand> FlockCommands = new List<FlockCommand>();
        public List<Vector3> FlockPositions = new List<Vector3>();
        public int MaxAnimalsInFlock = 8;
        public float FlockRadius = 3f;
        public float FlockAddRadius = 5f;
        public float DistanceProximity = 1.5f;

        public float flocksize;

        public FlockData FlockInfo = new FlockData();

        int FrameCounter = 0;
        int FrameInterval = 20;

        //UIScript animalcounter;
        //public int animalcount;
        //Countdown countdown;

        // Use this for initialization
        void Start() {
            FlockInfo.Flock = this;
            SubToEvents();
            //animalcounter = GameObject.Find("AnimalCounter").GetComponent<UIScript>();
            //animalcount = animalcounter.animalnumber;
            //countdown = GameObject.Find("Timer").GetComponent<Countdown>();
        }

        // Update is called once per frame
        void FixedUpdate() {
            if (FrameCounter != FrameInterval)
                ++FrameCounter;
            else
            {
                SearchForAnimalsInFlockRange();
                FrameCounter = 0;
            }
        }


        //
        void SearchForAnimalsInFlockRange()
        {
            if (!NodeManager.Instance.IsReady)
                return;

            Animal AnimalRef;
            for (int AnimalIndex = 0; AnimalIndex < NodeManager.Instance.ActiveAnimals.Count; ++AnimalIndex)
            {
                AnimalRef = NodeManager.Instance.ActiveAnimals[AnimalIndex];

                if (AnimalRef == null)
                {
                    NodeManager.Instance.ActiveAnimals.Remove(AnimalRef);
                    continue;
                    //break; // TODO: There shouldn't be any nulls in the animal list!
                }

                float Distance = Vector3.Distance(AnimalRef.transform.position, transform.position);
                if (Distance < FlockAddRadius && AnimalRef != ThisDog && AnimalRef.AnimalType != AnimalTypes.Fox)
                {
                    if (!AnimalsInFlock.Contains(AnimalRef))
                    {
                        AnimalAIManager AquiredAAIM = NodeManager.Instance.ActiveAnimals[AnimalIndex].GetComponent<AnimalAIManager>();
                        if(AquiredAAIM.ActiveCommand == null)
                            AquiredAAIM.SetFlockingCommand(this);
                    }
                }
            }
        }

        //
        public void AddToFlock(Animal _Animal, FlockCommand _FC)
        {
            if (_Animal == null)
                return;

            if (!AnimalsInFlock.Contains(_Animal))
            {
                AnimalsInFlock.Add(_Animal);
                FlockCommands.Add(_FC);
                SetFlockPoistion(_FC);
                UpdateFlockCount(_Animal, 1);
                EventManager.InvokeUISkillButtonUpdate(FlockInfo);
            }
        }

        //
        public void RemoveFromFlockEvent(Animal _Animal)
        {
            if (AnimalsInFlock.Contains(_Animal))
            {
                for (int FlockIndex = 0; FlockIndex < FlockCommands.Count; FlockIndex++)
                {
                    if (FlockCommands[FlockIndex].ThisAnimal == _Animal)
                    {
                        RemoveFromFlock(FlockCommands[FlockIndex]);                     
                    }
                }                    
            }
        }
               

        //
        public void RemoveFromFlock(FlockCommand _Command)
        {
            if (_Command == null)
                return;
            if (FlockCommands.Contains(_Command))
            {
                FlockCommands.Remove(_Command);
                AnimalsInFlock.Remove(_Command.ThisAnimal);
                UpdateFlockCount(_Command.ThisAnimal, -1);
                EventManager.InvokeUISkillButtonUpdate(FlockInfo);
            }
        }

        //
        public void SetAllFlockPositions()
        {
            FlockPositions.Clear();

            for (int FlockIndex = 0; FlockIndex < AnimalsInFlock.Count; ++FlockIndex)
            {
                FlockCommands.Add(AnimalsInFlock[FlockIndex].GetComponent<FlockCommand>());
            }

            for (int FlockIndex = 0; FlockIndex < FlockCommands.Count; ++FlockIndex)
                SetFlockPoistion(FlockCommands[FlockIndex]);
        }

        //
        void SetFlockPoistion(FlockCommand _FC)
        {
            bool PositionIsValid = false;
            GetRandomPosInCircle(_FC);

            while (!PositionIsValid)
            {
                GetRandomPosInCircle(_FC);
                PositionIsValid = CheckProxyToOthers(_FC);
            }
            FlockPositions.Add(_FC.FlockPosition);
        }

        //
        void GetRandomPosInCircle(FlockCommand _FC)
        {
            Vector2 RandomPoint = Random.insideUnitCircle;
            float Multiplier = Random.Range(1.5f, FlockRadius);
            RandomPoint = RandomPoint * Multiplier;
            _FC.FlockPosition = new Vector3(RandomPoint.x, 0f, RandomPoint.y);
        }

        //
        bool CheckProxyToOthers(FlockCommand _FC)
        {
            float DogDistance = Vector3.Distance(Vector3.zero, _FC.FlockPosition);
            if (DogDistance < DistanceProximity)
                return false;

            for (int FlockIndex = 0; FlockIndex < FlockPositions.Count; ++FlockIndex)
            {
                float FlockMemberDistance = Vector3.Distance(FlockPositions[FlockIndex], _FC.FlockPosition);
                if (FlockMemberDistance < DistanceProximity)
                    return false;
            }
            return true;
        }

        //
        void VaryFlockPosition(Vector3 _CurrentPosition)
        {

        }

        //
        void UpdateFlockCount(Animal _Animal, int _Amount)
        {
            switch(_Animal.AnimalType)
            {
                case AnimalTypes.Cow:
                    FlockInfo.CowsInFlock += _Amount;
                    break;
                case AnimalTypes.Chicken:
                    FlockInfo.ChickensInFlock += _Amount;
                    break;
                case AnimalTypes.Pig:
                    FlockInfo.PigsInFlock += _Amount;
                    break;
                case AnimalTypes.Sheep:
                    FlockInfo.SheepInFlock += _Amount;
                    break;
            }             
        }


        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, FlockRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, FlockAddRadius);
        }

        //private void OnCollisionEnter(Collision collision)
        //{
        //    if (collision.gameObject.CompareTag("Finish"))
        //    {
        //        //Debug.Log("Animals Saved : " + AnimalsInFlock.Count + "/" + animalcount);
        //        //Debug.Log("Time taken : " + (300 - countdown.timeLeft));
        //        // 300 can be changed to maxTimer, determined at Start of round
        //        float h = AnimalsInFlock.Count;

        //    }
        //}

        private void OnCollisionEnter(Collision dig)
        {
            if (dig.gameObject.CompareTag("digsite"))
            {
                print("h");
                float h = AnimalsInFlock.Count;

            }
        }

        public void SubToEvents()
        {
            EventManager.EntityDeReg += RemoveFromFlockEvent;
        }
        

    }

}