﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class PushObjectCommand : Command {

        Animal ThisAnimal;
        Rigidbody ThisAnimalRB;
        AnimalAIManager ThisAAM;
        public PushObject ThisPushObject;
        Transform OriginalPushObjectOwner;
        public InteractionCommandState CommandState = InteractionCommandState.Idle;
        public float CurrentTaskTime;
        public Vector3 PushObjectLookAtTarget;
        public float TargetLookTolerence = 1f;

        public float PushMovementModifier = 1f;
        public float PushRotationModifier = 1f;

        public override void OnEnterCommand()
        {
            ThisAnimal = ThisAAM.ThisAnimal;
            ThisAAM.SetMovementCommand(NodeManager.Instance.FindNodeFromWorldPosition( ThisPushObject.PushPosition));
            ThisAAM.ActiveCommand = this;
            ThisPushObject.PushInProgress = true;
            ThisAnimalRB = gameObject.GetComponent<Rigidbody>();
            PushObjectLookAtTarget = ThisPushObject.transform.position;
        }

        public override void OnExitCommand()
        {
            Decommission();
        }

        public override void Start()
        {

        }

        public override void Update()
        {
            if (CommandState == InteractionCommandState.UndertakingAction)
                CurrentTaskTime += Time.deltaTime;
        }

        public override void FixedUpdate()
        {
            switch (CommandState)
            {
                case InteractionCommandState.Idle:
                    if (ThisAAM.ActiveMovementCommand != null)
                    {
                        CommandState = InteractionCommandState.MovingToPosition;
                        transform.position = ThisPushObject.PushPosition;
                        //print("tp");
                    }
                    break;
                case InteractionCommandState.MovingToPosition:
                    float StartDist = Vector3.Distance(ThisAnimal.transform.position, ThisPushObject.PushPosition);
                    CommandState = InteractionCommandState.FacingTarget;
                    if (ThisAAM.ActiveMovementCommand == null && StartDist < 1f)
                    {
                        ThisAAM.transform.position = ThisPushObject.PushPosition;
                        
                        CommandState = InteractionCommandState.FacingTarget;
                    }
                    break;
                case InteractionCommandState.FacingTarget:
                    if (!CheckIfFacingTarget(TargetLookTolerence))
                        RotateTowardsTarget();
                        //FaceTarget();
                        //StartCoroutine(Facetarger());
                    else
                    {
                        print("pushin");
                        StopMovement();
                        StartCoroutine(waiting());
                        OriginalPushObjectOwner = ThisPushObject.transform.parent;
                        ThisPushObject.transform.SetParent(ThisAnimal.transform);
                        ThisPushObject.PushInProgress = true;
                        CommandState = InteractionCommandState.UndertakingAction;
                    }
                    break;
                case InteractionCommandState.UndertakingAction:
                    float EndDist = Vector3.Distance(gameObject.transform.position, ThisPushObject.PushEndPos);
                    if (EndDist < 0.3f)
                    {
                        print("done");
                        StopMovement();
                        ThisPushObject.transform.SetParent(OriginalPushObjectOwner);
                        CommandState = InteractionCommandState.ActionFinished;
                    }
                    else
                    {

                        //StartCoroutine(teleporttoEnd());
                        MoveToEndPos();
                        StartCoroutine(teleporttoEnd());
                    }
                    break;
                case InteractionCommandState.ActionFinished:
                    ThisPushObject.HasBeenPushed = true;
                    OnExitCommand();
                    break;
            }
        }
        public override void Decommission()
        {
            StopMovement();
            ThisAAM.ActiveCommand = null;
            Destroy(this);
        }

        public void PassInfo(AnimalAIManager _ThisAAM, PushObject _Object)
        {
            ThisPushObject = _Object;
            ThisAAM = _ThisAAM;
        }

        //Stops All movement
        void StopMovement()
        {
            Rigidbody RB = ThisAnimal.gameObject.GetComponent<Rigidbody>();
            RB.velocity = Vector3.zero;
            RB.isKinematic = true;
            //transform.rotation = Quaternion.Euler(transform.forward);
            RB.isKinematic = false;
        }

        IEnumerator InitatePushObject()
        {
            yield return new WaitUntil(() => ThisAAM.ActiveMovementCommand == null);

        }

        //Rotates the Owner of Command
        void RotateTowardsTarget()
        {
            Vector3 LookAtTarget = PushObjectLookAtTarget;

            float step = ThisAnimal.RotationSpeed*0.25f * Time.deltaTime;
            Vector3 targetDir = LookAtTarget - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 1f);
            transform.rotation = Quaternion.LookRotation(newDir);
        }

        //Uses Dot product of 2 vectors to deterime if facing target with Tolerence
        bool CheckIfFacingTarget(float _TargetLookTolerence)
        {
            Vector3 DirectionToWaypoint = (transform.position - PushObjectLookAtTarget).normalized;
            float DotProduct = Vector3.Dot(DirectionToWaypoint, transform.forward);
            if (DotProduct < (-1 + TargetLookTolerence) && DotProduct > (-1 + -TargetLookTolerence))
            {
                Quaternion LookRot = Quaternion.LookRotation(-(transform.position - PushObjectLookAtTarget));
                transform.rotation = LookRot;
                return true;
            }
            else
                return false;
        }

        void MoveToEndPos()
        {
            Vector3 WorldPos = ThisPushObject.PushEndPos;
            Vector3 LookAtTarget = new Vector3(WorldPos.x, 0, WorldPos.z);

            float step = (ThisAnimal.RotationSpeed* PushRotationModifier) * Time.deltaTime;
            Vector3 targetDir = LookAtTarget - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 1f);
            transform.rotation = Quaternion.LookRotation(newDir);
            ThisAnimalRB.velocity = transform.forward * (ThisAnimal.Speed * PushMovementModifier);
        }

        void FaceTarget()
        {
            Vector3 direction = (ThisPushObject.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }

        // lams code
        IEnumerator teleporttoEnd()
        {
            yield return new WaitForSeconds(.5f);
            transform.position = ThisPushObject.PushEndPos;
        }
        IEnumerator Facetarger()
        {
            yield return new WaitForSeconds(0f);
            Vector3 direction = (ThisPushObject.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = lookRotation; // Quaternion.Slerp(transform.rotation, lookRotation,  5f);
            
        }
        IEnumerator waiting()
        {
            yield return new WaitForSeconds(1);
        }
    }
}
