﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public abstract class InteractionCommand : Command
    {

        public Animal ThisAnimal;
        public AnimalAIManager ThisAAM;
        public InteractableObject ThisInteractionObject;
        public Vector3 ObjectLookAtTarget;
        public float TargetLookTolerence = 1f;

        //Stops All movement
        protected void StopMovement()
        {
            Rigidbody RB = ThisAnimal.gameObject.GetComponent<Rigidbody>();
            RB.velocity = Vector3.zero;
            RB.isKinematic = true;
            RB.isKinematic = false;
        }

        //Rotates the Owner of Command
        protected void RotateTowardsTarget()
        {
            Vector3 LookAtTarget = ObjectLookAtTarget;

            float step = ThisAnimal.RotationSpeed * 0.25f * Time.deltaTime;
            Vector3 targetDir = LookAtTarget - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);
        }

        //Uses Dot product of 2 vectors to deterime if facing target with Tolerence
        protected bool CheckIfFacingTarget(float _TargetLookTolerence)
        {
            Vector3 DirectionToWaypoint = (transform.position - ObjectLookAtTarget).normalized;
            float DotProduct = Vector3.Dot(DirectionToWaypoint, transform.forward);
            if (DotProduct < (-1 + TargetLookTolerence) && DotProduct > (-1 + -TargetLookTolerence))
            {
                Quaternion LookRot = Quaternion.LookRotation(-(transform.position - ObjectLookAtTarget));
                transform.rotation = LookRot;
                return true;
            }
            else
                return false;
        }
    }
}
