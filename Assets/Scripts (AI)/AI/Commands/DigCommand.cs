﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class DigCommand : InteractionCommand
    {
        public DigableObject ThisDiggableObject;
        public Transform MeshAndColliderTrans;
        public InteractionCommandState CommandState = InteractionCommandState.Idle;
        public GameObject spwnanimal;

        public float CurrentTaskTime;

        public override void OnEnterCommand()
        {
            ThisAnimal = ThisAAM.ThisAnimal;
            ThisAAM.SetMovementCommand(NodeManager.Instance.FindNodeFromWorldPosition(ThisDiggableObject.CurrentDigNode.GetWorldPos()));
            ThisAAM.ActiveCommand = this;
            ThisDiggableObject.DigInProgress = true;
            ObjectLookAtTarget = ThisDiggableObject.transform.position;
            MeshAndColliderTrans = ThisAnimal.transform.GetChild(0);
        }

        public override void OnExitCommand()
        {
            Decommission();
        }

        public override void Start()
        {

        }

        public override void Update()
        {
            if (CommandState == InteractionCommandState.UndertakingAction)
                CurrentTaskTime += Time.deltaTime;
        }

        public override void FixedUpdate()
        {
            switch (CommandState)
            {
                case InteractionCommandState.Idle:
                    if (ThisAAM.ActiveMovementCommand != null)
                    {
                        CommandState = InteractionCommandState.MovingToPosition;
                    }
                    break;
                case InteractionCommandState.MovingToPosition:
                    float StartDist = Vector3.Distance(ThisAnimal.transform.position, ThisDiggableObject.CurrentDigNode.GetWorldPos());
                    if (ThisAAM.ActiveMovementCommand == null && StartDist < 1f)
                    {
                        Vector3 GotVec = ThisDiggableObject.CurrentDigNode.GetWorldPos();
                        ThisAnimal.transform.position = new Vector3(GotVec.x, 0, GotVec.z);
                        CommandState = InteractionCommandState.FacingTarget;
                    }
                    break;
                case InteractionCommandState.FacingTarget:
                    if (!CheckIfFacingTarget(TargetLookTolerence))
                        RotateTowardsTarget();
                    else
                    {
                        StopMovement();
                        ThisDiggableObject.DigInProgress = true;
                        MeshAndColliderTrans.gameObject.SetActive(false);
                        CommandState = InteractionCommandState.UndertakingAction;
                    }
                    break;
                case InteractionCommandState.UndertakingAction:
                    if (CurrentTaskTime > ThisDiggableObject.DigTime)
                    {
                        TeliportToOtherNode();
                        ThisDiggableObject.SwitchDigDirection();
                        MeshAndColliderTrans.gameObject.SetActive(true);
                        CommandState = InteractionCommandState.ActionFinished;
                    }
                    break;
                case InteractionCommandState.ActionFinished:
                    ThisDiggableObject.DigInProgress = false;
                    OnExitCommand();
                    break;
            }
        }
        public override void Decommission()
        {
            StopMovement();
            ThisAAM.ActiveCommand = null;
            Destroy(this);
        }

        public void PassInfo(AnimalAIManager _ThisAAM, DigableObject _Object)
        {
            ThisDiggableObject = _Object;
            ThisAAM = _ThisAAM;
        }

        public void TeliportToOtherNode()
        {
            //Vector3 GotVec = ThisDiggableObject.CurrentExitNode.GetWorldPos();
            Vector3 GotVec = ThisDiggableObject.DigEndPosIndicator.transform.position;
            ThisAnimal.transform.position = new Vector3(GotVec.x, 0, GotVec.z);
            StartCoroutine(tpothers());
            
            TeleportOthers teleport = ThisDiggableObject.GetComponent<TeleportOthers>();
            //teleport.activated = true;

        }

        IEnumerator tpothers()
        {
            print("others");
            yield return new WaitForSeconds(1);
            //  spwnanimal.transform.position = ThisDiggableObject.DigEndPosIndicator.transform.position;
        }
    }
}