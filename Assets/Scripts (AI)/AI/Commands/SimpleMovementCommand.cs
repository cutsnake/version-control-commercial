﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovementCommand : Command
{
    public enum MovementStatus
    {
        Idle,
        MovingToDetourWaypoint,
        MovingToTarget,
        ReachedTarget,
        NoPath
    }

    public enum RotationStatus
    {
        NotLookingAtTarget,
        LookingAtTarget
    }

    [Header("Refrence Variables")]
    public Rigidbody ThisRB;
    public Collider ThisCollider;
    //public AnimalAIManager ThisAAM;

    [Space]
    [Header("Status")]
    public MovementStatus CurrentMovementStatus = MovementStatus.Idle;

    [Space]
    [Header("Waypoint Variables")]
    public Vector3 CurrentWaypoint;
    public Vector3 DetourWaypoint;
    public Vector3 WaypointLookAtTarget;

    [Space]
    [Header("Movement Variables")]
    public float Speed = 2f;
    public float RotationSpeed = 2f;
    public float WaypointTolerence = 0.1f;
    public float DetourWaypointTolerence = 0.5f;
    public float TargetLookTolerence = 0.001f;
    public float DirectionalMaxDistanceCheck = 4f;
    public bool NeedsToFaceTarget = false;

    [Space]
    [Header("Debugging")]
    public bool VisualDebugging = false;
    public float LookATDebug;

    //Acts as a constructor
    public void PassInfo(float _Speed, float _RotationSpeed, bool _NeedsToFaceEndTarget, Transform _MovementWaypoint, Transform _LookAtTarget)
    {
        NeedsToFaceTarget = _NeedsToFaceEndTarget;
        Speed = _Speed;
        RotationSpeed = _RotationSpeed;
        CurrentWaypoint = _MovementWaypoint.position;
        WaypointLookAtTarget = _LookAtTarget.position;
        OnEnterCommand();
    }

    public override void Start()
    {

    }

    public override void OnEnterCommand()
    {
        //ThisAAM = gameObject.GetComponent<AnimalAIManager>();
        //ThisAAM.ActiveMovementCommand = this;
        //ThisRB = GetComponent<Rigidbody>();
        //ThisCollider = GetComponent<Collider>();
        //StopMovement();
    }

    public override void Update()
    {

    }

    public override void FixedUpdate()
    {
        UndertakeAction();
    }

    public override void OnExitCommand()
    {
        Decommission();
    }


    //Drive the Actions Undertaken while command is attaced
    void UndertakeAction()
    {
        switch(CurrentMovementStatus)
        {
            case MovementStatus.Idle:
                if(PathToWaypointIsClear(transform.position, CurrentWaypoint))
                    CurrentMovementStatus = MovementStatus.MovingToTarget;
                else
                {
                    Vector3 Detour = IterativeDistanceCheck(CurrentWaypoint, DirectionalMaxDistanceCheck);
                    if (Detour != Vector3.zero)
                    {
                        DetourWaypoint = Detour;
                        CurrentMovementStatus = MovementStatus.MovingToDetourWaypoint;
                    }
                    else
                        CurrentMovementStatus = MovementStatus.NoPath;
                }
                break;
            case MovementStatus.MovingToDetourWaypoint:
                MoveTowardsWaypoint(DetourWaypoint);
                if(CheckIfAtWaypoint(DetourWaypoint, DetourWaypointTolerence))
                    CurrentMovementStatus = MovementStatus.MovingToTarget;

                break;
            case MovementStatus.MovingToTarget:
                MoveTowardsWaypoint(CurrentWaypoint);
                if (CheckIfAtWaypoint(CurrentWaypoint, WaypointTolerence))
                {
                    CurrentMovementStatus = MovementStatus.ReachedTarget;
                    StopMovement();
                }
                break;
            case MovementStatus.ReachedTarget:
                if(!NeedsToFaceTarget)
                    OnExitCommand();

                if (CheckIfFacingTarget(TargetLookTolerence))
                    OnExitCommand();
                else
                    RotateTowardsTarget();
                break;
            case MovementStatus.NoPath:
                Debug.Log("No Path!");
                OnExitCommand();
                break;
        }
    }


    //Checks if path To waypoint is unhindered
    bool PathToWaypointIsClear(Vector3 _OriginPosition, Vector3 _TargetPosition)
    {
        RaycastHit Hit;
        Vector3 TargetDir = _TargetPosition - _OriginPosition;
        Ray CheckRay = new Ray(_OriginPosition, TargetDir);
        float DistanceToWaypoint = Vector3.Distance(_TargetPosition, _OriginPosition) + 0.25f;

        bool DidHit = Physics.Raycast(CheckRay, out Hit, DistanceToWaypoint);
        if (!DidHit)
            return true;

        return false;
    }

    //Iterates different values to Directional check, checks the collider path and returns results
    Vector3 IterativeDistanceCheck(Vector3 _TargetWaypoint, float _MaxDistance)
    {
        float MaxDistanceFromTarget = _MaxDistance;
        for (float DistIndex = 1f; DistIndex < MaxDistanceFromTarget; DistIndex += 0.5f)
        {
            Vector3 DirectionCheck = DirectionalCheck(_TargetWaypoint, DistIndex);
            if (DirectionCheck != Vector3.zero)
            {
                return DirectionCheck;
            }
        }
        return Vector3.zero;
    }

    //Iterates in different directions checking if waypoint is clear
    Vector3 DirectionalCheck(Vector3 _TargetWaypoint, float _Distance)
    {
        float DistanceFromTarget = _Distance;
        for (int XIndex = -1; XIndex < 2; XIndex++)
            for (int ZIndex = -1; ZIndex < 2; ZIndex++)
            {
                if (XIndex == 0 && ZIndex == 0)
                    continue;

                Vector3 CheckVector = new Vector3(_TargetWaypoint.x + (XIndex * DistanceFromTarget), _TargetWaypoint.y, _TargetWaypoint.z + (ZIndex * DistanceFromTarget));
                if (PathToWaypointIsClear(transform.position, CheckVector))
                {
                    if (CheckIfColliderIsNotImpeaded(transform.position, CheckVector))
                    {
                        if (PathToWaypointIsClear(CheckVector, _TargetWaypoint))
                        {
                            if (CheckIfColliderIsNotImpeaded(CheckVector, _TargetWaypoint))
                            {
                                return CheckVector;
                            }
                        }
                    }
                    //if (PathToWaypointIsClear(CheckVector, _TargetWaypoint))
                    //    return CheckVector;
                }
            }
        return Vector3.zero;
     }
       
    //
    bool CheckIfColliderIsNotImpeaded(Vector3 _Origin, Vector3 _Waypoint)
    {
        bool IsNotImpeaded = true;
        float ColliderHalfGirth = ThisCollider.bounds.size.x;

        Vector3 Direction = _Origin - _Waypoint;
        Vector3 Vertical = _Waypoint + Vector3.up;
        Vector3 RightAngleFromDirection = Vector3.Cross(Direction, Vertical);
        Vector3 RightOfWaypoint = _Waypoint + (RightAngleFromDirection.normalized * ColliderHalfGirth);
        Vector3 LeftOfWaypoint = -_Waypoint + (RightAngleFromDirection.normalized * -ColliderHalfGirth);

        if (!PathToWaypointIsClear(_Origin, RightOfWaypoint))
            return false;
        if (!PathToWaypointIsClear(_Origin, LeftOfWaypoint))
            return false;

        return IsNotImpeaded;
    }


    //Check to see if current positon is within range of Waypoint Node to change state
    bool DistanceCheck(Vector3 _Waypoint, float _Tolerance)
    {
        if (Vector3.Distance(transform.position, _Waypoint) < _Tolerance)
            return true;
        else
            return false;
    }

    //Undertakes the applying of velocity ot the RB as well as the game object rotation
    void MoveTowardsWaypoint(Vector3 _Waypoint)
    {
        Vector3 LookAtTarget = _Waypoint;

        float Step = RotationSpeed * Time.deltaTime;
        Vector3 TargetDir = LookAtTarget - transform.position;
        Vector3 NewDir = Vector3.RotateTowards(transform.forward, TargetDir, Step, 0.0f);
        transform.rotation = Quaternion.LookRotation(NewDir);
        ThisRB.velocity = transform.forward * Speed;
    }

    //Calls a distance check to change movement status
    bool CheckIfAtWaypoint(Vector3 _Waypint, float _Tolerence)
    {
        if (DistanceCheck(_Waypint, _Tolerence))
        {
            return true;
        }
        else
            return false;
    }

    //Rotates the GO stationary
    void RotateTowardsTarget()
    {
        Vector3 LookAtTarget = WaypointLookAtTarget;

        float step = RotationSpeed * Time.deltaTime;
        Vector3 targetDir = LookAtTarget - transform.position;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    //Uses Dot product of 2 vectors to deterime if facing target with Tolerence
    bool CheckIfFacingTarget(float _TargetLookTolerence)
    {
        Vector3 DirectionToWaypoint = (transform.position - WaypointLookAtTarget).normalized;
        float DotProduct = Vector3.Dot(DirectionToWaypoint, transform.forward);
        LookATDebug = Vector3.Dot(DirectionToWaypoint, transform.forward);

        if (DotProduct < (-1 + TargetLookTolerence) && DotProduct > (-1 + -TargetLookTolerence))
            return true;
        else
            return false;
    }

    //Haults Movement
    void StopMovement()
    {
        ThisRB.velocity = Vector3.zero;
        ThisRB.isKinematic = true;
        //transform.rotation = Quaternion.Euler(transform.forward);
        ThisRB.isKinematic = false;
    }


    //Waitsfor Seconds to stagger PathRequests
    IEnumerator Wait(float _WaitTime)
    {
        yield return new WaitForSeconds(_WaitTime);
    }

    //For ease of use getting to destroy rather than recycle it... Could implement later but getting this all working first.
    public override void Decommission()
    {
        StopMovement();
        //ThisAAM.ActiveMovementCommand = null;
        Destroy(this);
    }

    //
    private void OnDrawGizmos()
    {
        float DistanceFromTarget = 2.5f;
        for (int XIndex = -1; XIndex < 2; XIndex++)
            for (int ZIndex = -1; ZIndex < 2; ZIndex++)
            {
                if (XIndex == 0 && ZIndex == 0)
                    continue;

                Vector3 CheckVector = new Vector3(CurrentWaypoint.x + (XIndex * DistanceFromTarget), CurrentWaypoint.y, CurrentWaypoint.z + (ZIndex * DistanceFromTarget));
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(CheckVector, 0.25f);

            }


        Gizmos.color = Color.green;
        if (DetourWaypoint != Vector3.zero)
            Gizmos.DrawWireSphere(DetourWaypoint, 0.5f);


        Vector3 Left = Vector3.Cross(transform.position, Vector3.up);
        Gizmos.DrawWireSphere(Left, 0.25f);

    }
}
