﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class FlockCommand : Command {

        public Animal ThisAnimal;
        public AnimalAIManager ThisAAIM;
        public Rigidbody ThisRb;
        public DogFlock Flock;

        public Vector3 FlockPosition;
        public Vector3 SeekTarget;
        public float DistanceProximity = 1.5f;
        public FlockingState CurrentFlockState = FlockingState.Idle;

        public Vector3 WonderPositon;
        public float WonderWaitTime;
        public float CurrentWonderWait;
        public float WonderRadius;

        public Vector3 targPoint;

        public override void OnEnterCommand()
        {
            ThisAAIM = GetComponent<AnimalAIManager>();
            ThisRb = GetComponent<Rigidbody>();
            ThisAnimal = GetComponent<Animal>();
            CurrentFlockState = FlockingState.Seeking;
            ThisAnimal.CurrentState = AnimalStatus.Flocking;
            WonderWaitTime = Random.Range(2.5f, 4.5f);
            WonderRadius = Flock.DistanceProximity - 0.5f;
        }

        public override void Start()
        {
            OnEnterCommand();
        }

        public override void Update()
        {

        }

        public override void FixedUpdate()
        {
            switch(CurrentFlockState)
            {
                case FlockingState.Idle:
                    MoniterDogPosition();
                    WonderWait();
                    break;
                case FlockingState.Wonder:
                    MoniterDogPosition();
                    Wonder(); 

                    break;

                case FlockingState.Seeking:
                    Seek();
                    if (Vector3.Distance(SeekTarget, transform.position) < 1.0f) // HACK: Hardcoded tollerance
                    {
                        StopMovement();
                        CurrentFlockState = FlockingState.Idle;
                    }
                    break;
                case FlockingState.Avoiding:

                    break;
            }
        }

        public override void OnExitCommand()
        {
            Decommission();
        }

        public override void Decommission()
        {
            Flock.RemoveFromFlock(this);
            ThisAnimal.CurrentState = AnimalStatus.Idle;
            ThisAAIM.ActiveCommand = null;
            Destroy(this);
        }

        public void PassInfo(DogFlock _Flock)
        {
            Flock = _Flock;
        }


        void ApplyFlockingRules()
        {

        }

        //
        void MoniterDogPosition()
        {
            if(Flock.ThisDog.CurrentState == AnimalStatus.Moving)
                StartCoroutine(DelayedStartSeek());
        }

        //
        void WonderWait()
        {
            if (CurrentWonderWait < WonderWaitTime)
                CurrentWonderWait += Time.deltaTime;
            else
            {
                CurrentWonderWait = 0;
                WonderWaitTime = Random.Range(2.5f, 4.5f);
                FindWonderToPosition();
                CurrentFlockState = FlockingState.Wonder;
            }
        }
        //
        void FindWonderToPosition()
        {
            Vector2 RandomPoint = Random.insideUnitCircle;
            float Multiplier = Random.Range(1.5f, WonderRadius);
            RandomPoint = RandomPoint * Multiplier;
            WonderPositon = new Vector3(RandomPoint.x, 0f, RandomPoint.y);
            WonderPositon = SeekTarget + WonderPositon;
        }

        //
        void Wonder()
        {
            Vector3 DesiredVel = ((WonderPositon - transform.position).normalized);
            Quaternion LookRot = Quaternion.LookRotation(DesiredVel);
            transform.rotation = Quaternion.Slerp(transform.rotation, LookRot, ((ThisAnimal.RotationSpeed * 0.25f) * Time.deltaTime));
            ThisRb.velocity = transform.forward * (ThisAnimal.CurrentSpeed * 0.25f);

            if (Vector3.Distance(transform.position , WonderPositon) < 0.4f)
            {
                StopMovement();
                CurrentFlockState = FlockingState.Idle;
            }
        }
 
        //
        IEnumerator DelayedStartSeek()
        { 
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            CurrentFlockState = FlockingState.Seeking;
        }

        void Seek()
        {
            SeekTarget = Flock.ThisDog.transform.position + FlockPosition;
            Vector3 DesiredVel = ((SeekTarget - transform.position).normalized);
            Quaternion LookRot = Quaternion.LookRotation(DesiredVel);
            transform.rotation = Quaternion.Slerp(transform.rotation, LookRot, (ThisAnimal.RotationSpeed * Time.deltaTime));

            ThisRb.velocity = transform.forward * ThisAnimal.CurrentSpeed;
        }

        //
        void StopMovement()
        {
            ThisRb.velocity = Vector3.zero;
            ThisRb.isKinematic = true;
            ThisRb.isKinematic = false;
        }


        float ReturnLargerCollider(Animal _Animal1, Animal _Animal2)
        {
            if (_Animal1.ColliderDistance >= _Animal2.ColliderDistance)
                return _Animal1.ColliderDistance;
            else
                return _Animal2.ColliderDistance;
        }
    }
}
