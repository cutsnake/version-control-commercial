﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class PigAIManager : AnimalAIManager
    {

        public void SetDigCommand(DigableObject _DiggableObject)
        {
            if (_DiggableObject == null)
                return;

            if (ActiveCommand != null)
            {
                ActiveCommand.OnExitCommand();
            }

            if (ThisAnimal == null)
            {
                ThisAnimal = this.gameObject.GetComponent<Animal>();
                ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            }

            DigCommand Push = gameObject.AddComponent<DigCommand>();
            Push.PassInfo(this, _DiggableObject);
            Push.OnEnterCommand();
            ActiveCommand = Push;
        }
    }
}