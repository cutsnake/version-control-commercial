﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class CowAIManager : AnimalAIManager
    {
        public void SetPushCommand(PushObject _PushObject)
        {
            if (_PushObject == null )
                return;
            if (ActiveCommand != null)
            {
                ActiveCommand.OnExitCommand();
            }
            if (ThisAnimal == null)
            {
                ThisAnimal = this.gameObject.GetComponent<Animal>();
                ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            }

            PushObjectCommand Push = gameObject.AddComponent<PushObjectCommand>();
            Push.PassInfo(this, _PushObject);
            Push.OnEnterCommand();
            ActiveCommand = Push;
        }

        public void SetRemoveHeavyCommand(RemovableObject _RemoveObject)
        {
            if (_RemoveObject == null )
                return;

            if (_RemoveObject.RemovableBy != ThisAnimal.AnimalType)
                return;
            if (ActiveCommand != null)
            {
                ActiveCommand.OnExitCommand();
            }

            if (ThisAnimal == null)
            {
                ThisAnimal = this.gameObject.GetComponent<Animal>();
                ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            }

            RemoveObjectCommand Remove = gameObject.AddComponent<RemoveObjectCommand>();
            Remove.PassInfo(this, _RemoveObject);
            Remove.OnEnterCommand();
            ActiveCommand = Remove;
        }
    }
}
