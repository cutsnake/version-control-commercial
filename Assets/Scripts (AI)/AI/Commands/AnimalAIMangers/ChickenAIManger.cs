﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class ChickenAIManger : AnimalAIManager
    {
        public void SetRemoveLightCommand(RemovableObject _RemoveObject)
        {
            if (_RemoveObject == null )
         
                
            return;

            if (_RemoveObject.RemovableBy != ThisAnimal.AnimalType)
           
                return;

            if (ActiveCommand != null)
            {
                ActiveCommand.OnExitCommand();
            }

            if (ThisAnimal == null)
            {
                Debug.Log("Light");
                ThisAnimal = this.gameObject.GetComponent<Animal>();
                ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            }

            RemoveObjectCommand Remove = gameObject.AddComponent<RemoveObjectCommand>();
            Remove.PassInfo(this, _RemoveObject);
            Remove.OnEnterCommand();
            ActiveCommand = Remove;
        }
    }
}