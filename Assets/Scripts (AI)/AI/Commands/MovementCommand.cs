﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <Notes>
    /// Need to Create a Path Avoidance for the detection of obsticals in path...
    /// Need to Create 
    /// </Notes>

    public class MovementCommand : Command
    {

        [Header("Refrence Variables")]
        public Animal ThisAnimal;
        public AnimalAIManager ThisAAM;
        public Rigidbody ThisRB;
        public Collider ThisCollider;

        [Space]
        [Header("Status")]
        public MovementStatus CurrentMovementStatus = MovementStatus.Idle;
        public PathRequestStatus CurrentPathStatus = PathRequestStatus.NoneRequested;

        [Space]
        [Header("Waypoint Variables")]
        public Node TargetNode;
        public List<Node> WayPoints;
        public Node TargetWaypoint;
        public Node CurrentWaypoint;
        public int CurrentWaypointIndex = 0;
        public float WaypointTolerence = 1.0f;
        public int WaypointObstructionDetectionTolerance = 4;

        [Space]
        [Header("Debugging")]
        public bool VisualDebugging = true;
        public int TestInt;

        public override void OnEnterCommand()
        {
            ThisRB = GetComponent<Rigidbody>();
            ThisCollider = GetComponent<Collider>();
            if (ThisCollider == null)
                ThisCollider = GetComponentInChildren<Collider>();

            StopMovement();
            //Node NodeCheck = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            //if(NodeCheck != null)
            //    ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            RequestPath(ThisAnimal.OccupiedNode, TargetNode, this);
            ThisAnimal.CurrentState = AnimalStatus.Moving;
        }

        public override void OnExitCommand()
        {
            Decommission();
        }

        public override void Start()
        {

        }

        public override void Update()
        {

        }

        public override void FixedUpdate()
        {
            MoveAlongPath();
        }

        public void PassInfo(Animal _Animal, Node _Destination, AnimalAIManager _AAM)
        {
            ThisAnimal = _Animal;
            TargetNode = _Destination;
            ThisAAM = _AAM;
        }

        //Recycle this/.. Need to create a CommandManager Recycler
        public override void Decommission()
        {
            StopMovement();
            ThisAAM.ActiveMovementCommand = null;
            ThisAnimal.CurrentState = AnimalStatus.Idle;
            Destroy(this);
        }

        #region Setup Functions

        //
        public void RequestPath(Node _StartNode, Node _TargetNode, MovementCommand _MovCom)
        {
            if (_TargetNode.IsOccupied)
            {
                OnExitCommand();
            }

            if (CurrentPathStatus == PathRequestStatus.NoneRequested)
            {
                CurrentPathStatus = PathRequestStatus.RequestedAndWaiting;

                Node ClosestUnobstrucedNode = ReturnClosestUnobstructedNode(_StartNode, _TargetNode);
                PathFinderManager.Instance.RequestPathFromNodes(ClosestUnobstrucedNode, _TargetNode, _MovCom);
            }
        }

        //
        Node ReturnClosestUnobstructedNode(Node _StartNode, Node _TargetNode)
        {
            RaycastHit Hit;
            Vector3 TargetDir = _TargetNode.GetWorldPos() - _StartNode.GetWorldPos();
            //Ray CheckRay = new Ray(_StartNode.GetWorldPos(), TargetDir);

            //Adoiding hitting own Collider
            float LargerExtent = ThisCollider.bounds.extents.z;
            if (ThisCollider.bounds.extents.x > ThisCollider.bounds.extents.z)
                LargerExtent = ThisCollider.bounds.extents.x;
            Vector3 BeyondColliderPos = TargetDir.normalized * (LargerExtent * 1.1f);

            Ray CheckRay = new Ray(_StartNode.GetWorldPos() + BeyondColliderPos, TargetDir);

            float DistanceToWaypoint = Vector3.Distance(_TargetNode.GetWorldPos(), _StartNode.GetWorldPos()) + 0.25f;

            bool DidHit = Physics.Raycast(CheckRay, out Hit, DistanceToWaypoint);
            if (!DidHit)
            {
                if (CheckIfColliderIsNotImpeaded(_StartNode.GetWorldPos(), _TargetNode.GetWorldPos()))
                    return _TargetNode;
            }
            RaycastHit Hit2;
            float NewDistanceToWaypoint = Vector3.Distance(_TargetNode.GetWorldPos(), _StartNode.GetWorldPos()) - 1f;
            bool DidHit2 = Physics.Raycast(CheckRay, out Hit2, NewDistanceToWaypoint);
            if (!DidHit2)
            {
                if (CheckIfColliderIsNotImpeaded(_StartNode.GetWorldPos(), _TargetNode.GetWorldPos()))
                    return NodeManager.Instance.FindNodeFromWorldPosition(Hit2.point);
            }
            return _StartNode;
        }

        //
        bool CheckIfColliderIsNotImpeaded(Vector3 _Origin, Vector3 _Waypoint)
        {
            bool IsNotImpeaded = true;
            float ColliderHalfGirth = ThisCollider.bounds.extents.x;

            Vector3 Direction = _Origin - _Waypoint;
            Vector3 Vertical = _Waypoint - (_Waypoint + Vector3.up);
            Vector3 RightAngleFromDirection = Vector3.Cross(Direction, Vertical).normalized;
            Vector3 RightOfWaypoint = _Waypoint + (RightAngleFromDirection * ColliderHalfGirth);
            Vector3 LeftOfWaypoint = _Waypoint + (-RightAngleFromDirection * ColliderHalfGirth);

            if (!PathToPointIsClear(_Origin, RightOfWaypoint))
                return false;
            if (!PathToPointIsClear(_Origin, LeftOfWaypoint))
                return false;

            return IsNotImpeaded;
        }

        //Checks if path To point is unhindered
        bool PathToPointIsClear(Vector3 _OriginPosition, Vector3 _TargetPosition)
        {
            RaycastHit Hit;
            Vector3 TargetDir = _TargetPosition - _OriginPosition;
            Ray CheckRay = new Ray(_OriginPosition, TargetDir);
            float DistanceToWaypoint = Vector3.Distance(_TargetPosition, _OriginPosition) + 0.25f;

            bool DidHit = Physics.Raycast(CheckRay, out Hit, DistanceToWaypoint);
            if (!DidHit)
                return true;

            return false;
        }

        //
        public void RecivePathRequest(PathRequest _CPR)
        {
            if (_CPR.PathIsFound == false)
            {
                CurrentPathStatus = PathRequestStatus.NoneRequested;
                StartCoroutine(Wait(0.25f));
                RequestPath(_CPR.StartingNode, _CPR.TargetNode, _CPR.Requestee);
            }
            else
            {
                WayPoints = _CPR.CompletedPath;
                CurrentWaypoint = WayPoints[0];
                CurrentPathStatus = PathRequestStatus.RecivedAndValid;
                CurrentMovementStatus = MovementStatus.MovingToNextWapoint;
            }
        }

        #endregion

        #region Active Functions

        //
        void MoveAlongPath()
        {
            if (WayPoints == null || CurrentPathStatus != PathRequestStatus.RecivedAndValid)
                return;

            switch (CurrentMovementStatus)
            {
                case MovementStatus.Idle:
                    StopMovement();
                    break;

                case MovementStatus.MovingToNextWapoint:

                    switch (WaypointCheck(WaypointTolerence))
                    {
                        case WaypointStatus.AtTargetNode:
                            CurrentMovementStatus = MovementStatus.ArrivedAtTargetNode;
                            break;
                        case WaypointStatus.AtWaypoint:
                            //Debug.Log("Changing Waypoints");
                            ThisAnimal.OccupiedNode = CurrentWaypoint;

                            if (CheckUpComingNodesForObstruction(WaypointObstructionDetectionTolerance) > 0)
                            {
                                //Debug.Log("Path Interupted");

                                /// OOO Here I need to find a path around and inject it into current path...
                                /// 
                                //
                                //
                                //

                                CurrentMovementStatus = MovementStatus.Idle;
                                break;
                            }

                            CurrentWaypoint = WayPoints[CurrentWaypointIndex];
                            CurrentMovementStatus = MovementStatus.MovingToNextWapoint;
                            break;
                        case WaypointStatus.BetweenWaypoints:
                            MoveToWaypoint();
                            break;
                    }
                    break;

                case MovementStatus.ArrivedAtTargetNode:
                    StopMovement();
                    ThisAnimal.OccupiedNode = CurrentWaypoint;
                    OnExitCommand();
                    break;
            }
        }
        #endregion


        #region Utility Functions

        //Scans to check if upcomming nodes have become occupied so there is no collisions
        int CheckUpComingNodesForObstruction(int _NumNodes)
        {
            int CheckExtent = CurrentWaypointIndex + _NumNodes;
            if (CheckExtent > WayPoints.Count)
                CheckExtent = (WayPoints.Count);

            for (int NodeIndex = CurrentWaypointIndex; NodeIndex < CheckExtent; ++NodeIndex)
            {
                TestInt = NodeIndex;
                if (WayPoints[NodeIndex].IsOccupied)
                    return NodeIndex;
            }
            return 0;
        }

        //
        List<Node> AvoidObsticle(List<Node> _OriginalWaypoints, Node _Obstruction)
        {
            //Need to include a stop if no path is found.
            //Patch gap between nodes index + and - 1 of obstruction Node
            //Need a call to the PathFinder here...
            //Request Obsticle Alteration... Let the Pathfinder handle it maybe...

            return null;
        }


        //Check to see if current positon is within range of Waypoint Node to change state
        WaypointStatus WaypointCheck(float _Tolerance)
        {
            float CurrentDisFromWaypoint = Vector3.Distance(ThisAnimal.transform.position, CurrentWaypoint.GetWorldPos());
            if (CurrentDisFromWaypoint < WaypointTolerence)
            {
                if (CurrentWaypoint == TargetNode)
                    return WaypointStatus.AtTargetNode;

                ++CurrentWaypointIndex;
                if (WayPoints[CurrentWaypointIndex] != null)
                    return WaypointStatus.AtWaypoint;
            }
            return WaypointStatus.BetweenWaypoints;
        }

        //Undertakes the applying of velocity ot the RB as well as the game object rotation
        void MoveToWaypoint()
        {
            Vector3 WorldPos = CurrentWaypoint.GetWorldPos();
            Vector3 LookAtTarget = new Vector3(WorldPos.x, ThisAnimal.GetComponent<Transform>().position.y, WorldPos.z); // HACK: Don't look at the actual ground, just above a bit

            float step = ThisAnimal.RotationSpeed * Time.deltaTime;
            Vector3 targetDir = LookAtTarget - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);
            ThisRB.velocity = transform.forward * ThisAnimal.Speed;
        }

        //Haults Movement
        void StopMovement()
        {
            ThisRB.velocity = Vector3.zero;
            ThisRB.isKinematic = true;
            ThisRB.angularVelocity = Vector3.zero;
            //transform.rotation = Quaternion.Euler(transform.forward);
            ThisRB.isKinematic = false;

        }

        //Waitsfor Seconds to stagger PathRequests
        IEnumerator Wait(float _WaitTime)
        {
            yield return new WaitForSeconds(_WaitTime);
        }

        //
        public void InteruptMovement()
        {
            OnExitCommand();
        }

        #endregion


        #region Visual Debugging

        //
        private void OnDrawGizmos()
        {
            if (VisualDebugging == false)
                return;
            if (WayPoints != null)
            {
                float WireSphereSize = Node.TileSize.x / 4;
                for (int NodeIndex = 0; NodeIndex < WayPoints.Count; ++NodeIndex)
                {
                    Gizmos.color = Color.green;
                    if (WayPoints[NodeIndex] == CurrentWaypoint)
                        Gizmos.color = Color.yellow;
                    Gizmos.DrawWireSphere(WayPoints[NodeIndex].GetWorldPos(), WireSphereSize);

                    if (CurrentWaypoint == null)
                        return;
                }
            }
        }

        #endregion
    }
}
