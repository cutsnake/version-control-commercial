﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AnimalAIManager : MonoBehaviour
    {
        public Animal ThisAnimal;
        public Command ActiveCommand;
        public MovementCommand ActiveMovementCommand;

        private void Start()
        {
            ThisAnimal = this.gameObject.GetComponent<Animal>();
        }

        //
        public void SetMovementCommand(Node _Destination)
        {
            if (_Destination == null)
                return;

            if(ThisAnimal == null)
            {
                ThisAnimal = this.gameObject.GetComponent<Animal>();
                ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            }

            if(ActiveMovementCommand != null)
                ActiveMovementCommand.InteruptMovement();

            ActiveMovementCommand = gameObject.AddComponent<MovementCommand>();
            ActiveMovementCommand.PassInfo(ThisAnimal, _Destination, this);
            ActiveMovementCommand.OnEnterCommand();
        }

        //
        public void SetFlockingCommand(DogFlock _Flock)
        {
            if (_Flock == null || ActiveCommand != null)
                return;
            if (ThisAnimal == null)
            {
                ThisAnimal = this.gameObject.GetComponent<Animal>();
                ThisAnimal.OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(ThisAnimal.transform.position);
            }

            FlockCommand FlockCommand = gameObject.AddComponent<FlockCommand>();
            FlockCommand.PassInfo(_Flock);
            ActiveCommand = FlockCommand;
            _Flock.AddToFlock(ThisAnimal, FlockCommand);
        }
    }
}
