﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class RemoveObjectCommand : InteractionCommand
    {
        public RemovableObject ThisRemoveObject;
        public InteractionCommandState CommandState = InteractionCommandState.Idle;
        public float CurrentRemoveTime;

        public override void OnEnterCommand()
        {
            
            ThisAnimal = ThisAAM.ThisAnimal;
            ThisAAM.SetMovementCommand(NodeManager.Instance.FindNodeFromWorldPosition(ThisRemoveObject.RemovePosition));
            ThisAAM.ActiveCommand = this;
            ThisRemoveObject.RemoveInProgress = true;
            ObjectLookAtTarget = ThisRemoveObject.transform.position;
        }

        public override void OnExitCommand()
        {
            Decommission();
        }

        public override void Start()
        {

        }

        public override void Update()
        {
            if (CommandState == InteractionCommandState.UndertakingAction)
                CurrentRemoveTime += Time.deltaTime;
        }

        public override void FixedUpdate()
        {
            switch (CommandState)
            {
                case InteractionCommandState.Idle:
                    if (ThisAAM.ActiveMovementCommand != null)
                    {
                        CommandState = InteractionCommandState.MovingToPosition;
                        // teleports animal to interaction point
                        //transform.position = ThisRemoveObject.RemovePosition;
                    }
                    break;
                case InteractionCommandState.MovingToPosition:
                    float StartDist = Vector3.Distance(ThisAnimal.transform.position, ThisRemoveObject.RemovePosition);
                    if (ThisAAM.ActiveMovementCommand == null && StartDist < 2f)
                    {
                        ThisAAM.transform.position = ThisRemoveObject.RemovePosition;
                        CommandState = InteractionCommandState.FacingTarget;
                    }
                    break;
                case InteractionCommandState.FacingTarget:
                    if (!CheckIfFacingTarget(TargetLookTolerence))
                        RotateTowardsTarget();
                    else
                    {
                        StopMovement();
                        //Undo comment to set remove object as child or animal
                        //ThisRemoveObject.transform.SetParent(ThisAnimal.transform);
                        ThisRemoveObject.RemoveInProgress = true;
                        CommandState = InteractionCommandState.UndertakingAction;
                    }
                    break;
                case InteractionCommandState.UndertakingAction:
                    if(CurrentRemoveTime > ThisRemoveObject.RemovalTime)
                    {
                        Destroy(ThisRemoveObject.gameObject);
                        CommandState = InteractionCommandState.ActionFinished;
                    }                 
                    break;
                case InteractionCommandState.ActionFinished:
                    OnExitCommand();
                    break;
            }
        }
        public override void Decommission()
        {
            StopMovement();
            ThisAAM.ActiveCommand = null;
            Destroy(this);
        }

        public void PassInfo(AnimalAIManager _ThisAAM, RemovableObject _Object)
        {
            ThisRemoveObject = _Object;
            ThisAAM = _ThisAAM;
        }
    }
}