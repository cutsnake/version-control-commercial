﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class PushObject : InteractableObject
    {
        public Vector3 PushPosition;
        public Vector3 PushEndPos;
        public Node PushPosNode;
        public Node EndPosNode;
        public float PushTime = 4;
        public bool HasBeenPushed = false;

        public GameObject PushPosiitonIndicator;
        public GameObject PushEndPosIndicator;

        public bool PushInProgress;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            DestroyIndicators();
        }

        //
        public void DestroyIndicators()
        {
            //if (PushPosiitonIndicator != null)
            //    Destroy(PushPosiitonIndicator);
            //if(PushEndPosIndicator != null)
            //    Destroy(PushEndPosIndicator);
        }
        public void SetIndicators(GameObject _From, GameObject _To)
        {
            PushPosiitonIndicator = _From;
            PushEndPosIndicator = _To;
            //PushPosNode = NodeManager.Instance.FindNodeFromWorldPosition(PushPosition);
            //EndPosNode = NodeManager.Instance.FindNodeFromWorldPosition(PushEndPos);
        }
        public GameObject[] GetIndicators()
        {
            GameObject[] GOS = new GameObject[2];
            GOS[0] = PushPosiitonIndicator;
            GOS[1] = PushEndPosIndicator;
            if (PushPosiitonIndicator == null || PushEndPosIndicator == null)
                return null;

            return GOS;
        }
    }
}
