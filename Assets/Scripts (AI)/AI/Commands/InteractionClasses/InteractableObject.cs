﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class InteractableObject : Entity
    {
        public Collider ThisCollider;
        public Node[] OccupyingNodes;

        // Use this for initialization
        protected override void Start()
        {
            EventManager.InvokeOjectReg(this);
            ThisCollider = GetComponent<Collider>();
            if(ThisCollider == null)
                ThisCollider = GetComponentInChildren<Collider>();
            if (ThisCollider == null)
                ThisCollider = GetComponentInParent<Collider>();
        }
    }
}
