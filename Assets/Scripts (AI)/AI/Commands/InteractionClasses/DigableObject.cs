﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class DigableObject : MonoBehaviour
    {
        public float DigTime;
        public Node CurrentDigNode;
        public Node CurrentExitNode;

        public DigDirection CurrentDirection = DigDirection.StartToEnd;

        public bool DigInProgress = false;
        public bool NodesSet = false;

        public GameObject DigPosiitonIndicator;
        public GameObject DigEndPosIndicator;

        // Use this for initialization
        protected void Start()
        {
            EventManager.InvokeDigSiteReg(this);
            //DestroyIndicator();
        }

        //
        public void DestroyIndicator()
        {
            if (DigPosiitonIndicator != null)
                Destroy(DigPosiitonIndicator);
            if (DigEndPosIndicator != null)
                Destroy(DigEndPosIndicator);
        }

        //
        public void SetIndicators(GameObject _From, GameObject _To)
        {
            DigPosiitonIndicator = _From;
            DigEndPosIndicator = _To;
        }

        //
        public GameObject[] GetIndicators()
        {
            GameObject[] GOS = new GameObject[2];
            GOS[0] = DigPosiitonIndicator;
            GOS[1] = DigEndPosIndicator;
            if (DigPosiitonIndicator == null || DigEndPosIndicator == null)
                return null;

            return GOS;
        }

        //
        public void SetAllNodes()
        {
            if(NodesSet == false)
            {
                CurrentDigNode = NodeManager.Instance.FindNodeFromWorldPosition(DigPosiitonIndicator.transform.position);
                CurrentExitNode = NodeManager.Instance.FindNodeFromWorldPosition(DigEndPosIndicator.transform.position);
                NodesSet = true;
            }
        }

        //
        public void SwitchDigDirection()
        {
            Node Temp;
            switch(CurrentDirection)
            {
                case DigDirection.StartToEnd:
                    Temp = CurrentDigNode;
                    CurrentDigNode = CurrentExitNode;
                    CurrentExitNode = Temp;
                    CurrentDirection = DigDirection.EndToStart;
                    break;
                case DigDirection.EndToStart:
                    Temp = CurrentDigNode;
                    CurrentDigNode = CurrentExitNode;
                    CurrentExitNode = Temp;
                    CurrentDirection = DigDirection.StartToEnd;
                    break;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(CurrentDigNode.GetWorldPos(), new Vector3(.5f,.5f,.5f));
        }
    }
}
