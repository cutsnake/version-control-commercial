﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class RemovableObject : InteractableObject
    {
        public float RemovalTime;
        public Vector3 RemovePosition = Vector3.zero;
        public Node RemovePosNode;
        public bool HasBeenRemoved = false;
        public bool RemoveInProgress = false;
        public AnimalTypes RemovableBy = AnimalTypes.Null;

        public GameObject RemovePosiitonIndicator;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            DestroyIndicator();
        }

        public void DestroyIndicator()
        {
            if (RemovePosiitonIndicator != null)
            {
                //Destroy(RemovePosiitonIndicator);
            }

        }
        public void SetIndicators(GameObject _RemovePoint)
        {
            RemovePosiitonIndicator = _RemovePoint;
            RemovePosNode = NodeManager.Instance.FindNodeFromWorldPosition(RemovePosition);
        }
        public GameObject GetIndicators()
        {
            GameObject GOS = RemovePosiitonIndicator;
            if (RemovePosiitonIndicator == null )
                return null;
            return GOS;
        }
    }
}
