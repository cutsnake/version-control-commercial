﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <Notes>
    /// Unit movement strucure needs overhaul, current simple quick, dirty implemetation;
    /// </Notes>

    public class Pointer : MonoBehaviour {

        public GameObject SelectionPointer;
        public GameObject SelectionPointer2;
        public Camera CamRef;

        public Vector3 MouseWorldPos = new Vector3();
        public Vector2 MouseCameraPos = new Vector2();

        public Node CurrentTouchNode;
        public InteractableObject CurrentSelectedIO;

        public AnimalAIManager CurrentDogAI;
        //Entity DogEntity;
        public FlockData CurrentFlockData;

        public Vector3 MousePosOffset = new Vector3(0,0.5f,0);
        public Vector3 MouseCursorOffset = new Vector3(0, 0.25f, 0);
        public bool ButtonIsBeingSelected = false;
        public SkillSelection CurrentSelectedSkill = SkillSelection.Null;

        public PushObject SelectedPushObject;
        public RemovableObject SelectedRemoveObject;
        public DigableObject SelectedDiggableObject;

        // Use this for initialization
        void Start() {
            CamRef = Camera.main;
            //DogEntity = CurrentDogAI.GetComponent<Animal>();
            SubToEvents();
        }

        // Update is called once per frame
        void Update() {

            if (!NodeManager.Instance.IsReady)
                return;

            MouseWorldPos = GetMouseWorldPoint();

            if(Input.GetMouseButtonDown(0))
            {
                NodeTouchSelection(MouseWorldPos);
                RequestDogMove();
                IniateSkill();
                EventManager.InvokeClearSkillSelection();
            }
        }

        //Converts sceen mouse point to world vector3
        Vector3 GetMouseWorldPoint()
        {
            MouseCameraPos.x = Input.mousePosition.x;
            MouseCameraPos.y = Input.mousePosition.y;
            RaycastHit CastHit;

            Ray CheckRay = CamRef.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(CheckRay, out CastHit))
            {
                return CastHit.point;
            }
            else
                return MouseWorldPos;
        }

        //
        void NodeTouchSelection(Vector3 _TouchPoint)
        {
            if (!NodeManager.Instance.IsReady)
                return;

            Node TouchNode = NodeManager.Instance.FindNodeFromWorldPosition(_TouchPoint);
            if (TouchNode != null)
            {
                if (CurrentTouchNode == TouchNode)
                    return;

                CurrentTouchNode = TouchNode;

                if (TouchNode.IsOccupied && CurrentSelectedSkill != SkillSelection.Null || CurrentSelectedSkill == SkillSelection.Dig)
                {
                    HandleObjectSelection(TouchNode);
                }
                else if (ButtonIsBeingSelected == false)
                {
                    SelectionPointer.SetActive(true);
                    SelectionPointer.transform.position = CurrentTouchNode.GetWorldPos() - MouseCursorOffset;
                    CurrentSelectedIO = null;
                    SelectionPointer.transform.localScale = new Vector3(1, 0.1f, 1);
                }

            }
            SetObjects();
        }

        //
        void HandleObjectSelection(Node _SelectedNode)
        {
            if (_SelectedNode.Occupant != null)
            {
                CurrentSelectedIO = _SelectedNode.Occupant.GetComponent<InteractableObject>();
                if (CurrentSelectedIO != null)
                {
                    SelectionPointer.SetActive(true);

                    float ScaleSize = CurrentSelectedIO.ThisCollider.bounds.size.z;
                    if (CurrentSelectedIO.ThisCollider.bounds.size.x > CurrentSelectedIO.ThisCollider.bounds.size.z)
                        ScaleSize = CurrentSelectedIO.ThisCollider.bounds.size.x;

                    SelectionPointer.transform.localScale = new Vector3(ScaleSize + 1, SelectionPointer.transform.localScale.y, ScaleSize + 1);
                    SelectionPointer.transform.position = _SelectedNode.Occupant.transform.position;
                }
            }
            else
            {
                DigableObject RetrunedDO = NodeManager.Instance.ReturnNodeDigObject(_SelectedNode);
                if(RetrunedDO != null)
                {
                    SelectionPointer.SetActive(true);

                    SelectionPointer.transform.position = RetrunedDO.CurrentDigNode.GetWorldPos();
                    SelectionPointer2.transform.position = RetrunedDO.CurrentExitNode.GetWorldPos();
                    SelectedDiggableObject = RetrunedDO;
                }
            }






        }

        //When selection of a UI Button is not underway, moves attached Dog
        void RequestDogMove()
        {
            if (CurrentTouchNode != null && CurrentDogAI != null && CurrentSelectedIO == null && ButtonIsBeingSelected == false && SelectedDiggableObject == null)
            {
                CurrentDogAI.SetMovementCommand(CurrentTouchNode);
                CurrentTouchNode = null;
                SelectionPointer.SetActive(false);
                SelectionPointer2.SetActive(false);
            }
        }

        //Monitors SkillSelection for actions
        void IniateSkill()
        {
            if (CurrentSelectedSkill == SkillSelection.Null)
                return;

            if (ButtonIsBeingSelected == true || CurrentFlockData == null)
                return;

            if (CurrentSelectedIO == null && SelectedDiggableObject == null)
                return;

            Animal RetrivedAnimal = null;
            List<Animal> Flock = CurrentFlockData.Flock.AnimalsInFlock;
            switch (CurrentSelectedSkill)
            {
                case SkillSelection.Push:
                    Debug.Log("Boooh");
                    if (CurrentFlockData.CowsInFlock > 0 && SelectedPushObject != null)
                    {
                        Debug.Log("Mooin");
                        RetrivedAnimal = GetAnimalType(Flock, AnimalTypes.Cow);
                        CowAIManager CowAI = RetrivedAnimal.GetComponent<CowAIManager>();
                        if(CowAI != null)
                        {
                            CowAI.SetPushCommand(SelectedPushObject);
                            Debug.Log("Mooohoooovin");
                        }
                            
                        SelectedPushObject = null;
                        CurrentTouchNode = null;
                    }             
                    break;
                case SkillSelection.RemoveLight:
                    if (CurrentFlockData.ChickensInFlock > 0 && SelectedRemoveObject != null)
                    {
                        Debug.Log("Light");
                        if (SelectedRemoveObject.RemovableBy != AnimalTypes.Chicken)
                            return;
                        Debug.Log("Bok");

                        RetrivedAnimal = GetAnimalType(Flock, AnimalTypes.Chicken);
                        ChickenAIManger ChickenAI = RetrivedAnimal.GetComponent<ChickenAIManger>();
                        if (ChickenAI != null)
                        {
                            ChickenAI.SetRemoveLightCommand(SelectedRemoveObject);
                            Debug.Log("Bokbok");
                        }

                        
                        SelectedRemoveObject = null;
                        CurrentTouchNode = null;
                    }
                    break;
                case SkillSelection.RemoveHeavy:
                    Debug.Log("Meow");
                    if (CurrentFlockData.CowsInFlock > 0 && SelectedRemoveObject != null)
                    {
                        Debug.Log("Moo");
                        if (SelectedRemoveObject.RemovableBy != AnimalTypes.Cow)
                            return;
                        Debug.Log("Moooo");
                        RetrivedAnimal = GetAnimalType(Flock, AnimalTypes.Cow);
                        CowAIManager CowAI = RetrivedAnimal.GetComponent<CowAIManager>();
                        if (CowAI != null)
                        {
                            CowAI.SetRemoveHeavyCommand(SelectedRemoveObject);
                            Debug.Log("Moooooo");
                        }

                        SelectedRemoveObject = null;
                        CurrentTouchNode = null;
                    }
                    break;
                case SkillSelection.Dig:
                    
                    Debug.Log("guhii");
                    if (CurrentFlockData.PigsInFlock > 0 && SelectedDiggableObject != null)
                    {
                      
                        Debug.Log("oink");
                        RetrivedAnimal = GetAnimalType(Flock, AnimalTypes.Pig);
                        PigAIManager PigAI = RetrivedAnimal.GetComponent<PigAIManager>();
                        if (PigAI != null)
                            PigAI.SetDigCommand(SelectedDiggableObject);
                        Debug.Log("guhiihii");
                        SelectedDiggableObject = null;
                        CurrentTouchNode = null;
                    }
                    break;
            }
        }

        //Searches through animal list to return type
        Animal GetAnimalType(List<Animal> _Flock, AnimalTypes _Type)
        {
            for (int AnimalIndex = 0; AnimalIndex < _Flock.Count; ++AnimalIndex)
            {
                if (_Flock[AnimalIndex].AnimalType == _Type)
                    return _Flock[AnimalIndex];
            }
            return null;
        }


        //Gets InteractableObjectsand sets them
        void SetObjects()
        {
            SelectedPushObject = null;
            SelectedRemoveObject = null;
            


            if (CurrentSelectedIO != null)
            {
                PushObject Push = CurrentSelectedIO.GetComponent<PushObject>();
                RemovableObject Remove = CurrentSelectedIO.GetComponent<RemovableObject>();
                
               
                if (Push != null)
                    SelectedPushObject = Push;
                else if (Remove != null)
                    SelectedRemoveObject = Remove;
                
            }
        }

        #region Event Functions

        //
        void SubToEvents()
        {
            EventManager.UIButtonSelected += ToggleButtonSelection;
            EventManager.ReciveSelectedSkill += ToggleSkillSelection;
            EventManager.FlockDataReg += UpdateFlockData;
        }

        //
        public void ToggleButtonSelection(bool _Selection)
        {
            ButtonIsBeingSelected = _Selection;
        }
        public void UpdateFlockData(FlockData _Data)
        {
            CurrentFlockData = _Data;
        }
        public void ToggleSkillSelection(SkillSelection _Selection)
        {
            CurrentSelectedSkill = _Selection;
        }

        #endregion

        ////
        //void MoveHoveredOverNode()
        //{
        //    if (!NodeManager.Instance.IsReady)
        //        return;
        //    Node HoveredOverNode = NodeManager.Instance.FindNodeFromWorldPosition(MouseWorldPos);
        //    if (HoveredOverNode != null)
        //    {
        //        if (CurrentHoverNode == HoveredOverNode)
        //        {
        //            if(!GridHover)
        //            {
        //                if (CurrentHoverNode.Occupant != null)
        //                    HoverOverPointer.transform.position = new Vector3(CurrentHoverNode.Occupant.transform.position.x, 0.3f, CurrentHoverNode.Occupant.transform.position.z);
        //                else
        //                    HoverOverPointer.transform.position = MouseWorldPos;
        //            }
        //            return;
        //        }

        //        HoverOverPointer.SetActive(true);
        //        CurrentHoverNode = HoveredOverNode;

        //        if (GridHover)
        //            HoverOverPointer.transform.position = CurrentHoverNode.GetWorldPos() - MouseCursorOffset;
        //        else
        //        {
        //            if(CurrentHoverNode.Occupant != null)
        //            {
        //                if(CurrentHoverNode.Occupant != DogEntity)
        //                    HoverOverPointer.transform.position = CurrentHoverNode.Occupant.transform.position;
        //            }
        //            else
        //                HoverOverPointer.transform.position = MouseWorldPos;
        //        }
        //    }
        //    else
        //        HoverOverPointer.SetActive(false);
        //}

        //
        //void NodeSelection()
        //{
        //    if (!NodeManager.Instance.IsReady)
        //        return;
        //    if (CurrentHoverNode != null)
        //    {
        //        if (CurrentSelectedNode == CurrentHoverNode)
        //            return;

        //        CurrentSelectedNode = CurrentHoverNode;
        //        SelectionPointer.SetActive(true);
        //        SelectionPointer.transform.position = CurrentSelectedNode.GetWorldPos() - MouseCursorOffset;
        //        //if (CurrentSelectedNode.IsOccupied && CurrentSelectedNode.Occupant != null)
        //        //{
        //        //    AnimalAIManager AquiredAAM = CurrentSelectedNode.Occupant.GetComponent<AnimalAIManager>();
        //        //    if (AquiredAAM != CurrentDogAI)
        //        //        CurrentSelectedNodeAnimal = AquiredAAM;
        //        //}
        //        //else
        //        //    CurrentSelectedNodeAnimal = null;

        //    }
        //    if ((CurrentHoverNode == null) || Input.GetMouseButtonDown(1) && CurrentSelectedNode != null)
        //    {
        //        if(CurrentSelectedNode != null)
        //        {
        //            if(CurrentSelectedNode.Occupant == null)
        //                CurrentSelectedNode = null;
        //        }
        //        SelectionPointer.SetActive(false);
        //    }
        //}
    }
}
