﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionRotate : MonoBehaviour {

    public float RotateSpeed = 1;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up, RotateSpeed * Time.deltaTime);
    }
}
