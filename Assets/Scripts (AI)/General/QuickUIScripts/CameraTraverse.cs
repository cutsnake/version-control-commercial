﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <Notes>
    /// Quick, dirty, inefficent code that gets stuff working
    /// </Notes>

    public class CameraTraverse : MonoBehaviour {

        public float MovementSpeed = 2.5f;
        public float ZoomSpeed = 0.5f;
        public float MinCameraZoom = 5;
        public float MaxCameraZoom = 10;
        public float RotateSpeed = 25;

        public Animal FollowingAnimal;
        public float Smoothing = 5;
        public float SnapSpeed = 5;

        public CameraInputState InputState = CameraInputState.KeyboardInput;
        public FollowState CameraState = FollowState.FollowingAnimal;

        //Keyboard Input Variables
        Vector2 Movement = new Vector2();
        RotationDirection RotationDir = RotationDirection.Null;
        float ZoomDelta;

        // Use this for initialization
        void Start() {

        }

        void Update() {

            MonitorKeyboardInput();

            //switch (CameraState)
            //{
            //    case FollowState.FollowingAnimal:
            //        FollowAnimal();
            //        if(Movement != Vector2.zero)
            //            CameraState = FollowState.IndependentMovement;
            //        break;
            //    case FollowState.IndependentMovement:
            //        MoveCamera(Movement);
            //        break;
            //    case FollowState.ReturningToAnimal:
            //        SmoothSnapToSelectedAnimal(CameraState);
            //        break;
            //}
            //RotateCamera(RotationDir);
            //ZoomCamera(ZoomDelta);
        }
        private void LateUpdate()
        {
            switch (CameraState)
            {
                case FollowState.FollowingAnimal:
                    FollowAnimal();
                    if (Movement != Vector2.zero)
                        CameraState = FollowState.IndependentMovement;
                    break;
                case FollowState.IndependentMovement:
                    MoveCamera(Movement);
                    break;
                case FollowState.ReturningToAnimal:
                    SmoothSnapToSelectedAnimal(CameraState);
                    break;
            }
            RotateCamera(RotationDir);
            ZoomCamera(ZoomDelta);
            ResetValues(); 
        }

        //
        void MonitorKeyboardInput()
        {
            if (InputState != CameraInputState.KeyboardInput)
                return;

            Movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            if (Input.GetKey(KeyCode.Q))
                RotationDir = RotationDirection.AntiClockwise;
            if (Input.GetKey(KeyCode.E))
                RotationDir = RotationDirection.Clockwise;
            ZoomDelta = Input.mouseScrollDelta.y;
            if (Input.GetKeyDown(KeyCode.Space) && FollowingAnimal != null)
                CameraState = FollowState.ReturningToAnimal;
        }

        //
        public void ChangeState(FollowState _State)
        {
            CameraState = _State;

        }

        //
        void MoveCamera(Vector2 _Input)
        {
            CameraState = FollowState.IndependentMovement;

            Vector3 Vertical = transform.forward * _Input.y;
            Vector3 Horizontal = transform.right * _Input.x;
            Vector3 Movement = Vertical + Horizontal;
            transform.position += Movement * MovementSpeed * Time.deltaTime;
        }
        public void MoveCameraPublic(Vector2 _Input)
        {
            Movement = _Input;
        }

        //
        void ZoomCamera(float _ZoomDelta)
        {
            if (_ZoomDelta != 0)
            {
                float NewZoom = Input.mouseScrollDelta.y * ZoomSpeed;
                NewZoom = -NewZoom + transform.position.y;

                if (NewZoom < MinCameraZoom)
                    NewZoom = MinCameraZoom;
                if (NewZoom > MaxCameraZoom)
                    NewZoom = MaxCameraZoom;

                transform.position = new Vector3(transform.position.x, NewZoom, transform.position.z);
            }
        }
        public void ZoomCameraPublic(float _ZoomDelta)
        {
            ZoomDelta = _ZoomDelta;
        }

        //
        void FollowAnimal()
        {
            if (FollowingAnimal != null)
                transform.position = FollowingAnimal.transform.position - new Vector3(0,FollowingAnimal.transform.position.y, 0);
        }

        //
        void RotateCamera(RotationDirection _Direction)
        {
            switch(_Direction)
            {
                case RotationDirection.Clockwise:
                    transform.Rotate(Vector3.up, RotateSpeed * Time.deltaTime);
                    break;
                case RotationDirection.AntiClockwise:
                    transform.Rotate(Vector3.up, -RotateSpeed * Time.deltaTime);
                    break;
                case RotationDirection.Null:

                    break;
            }
        }
        public void RotateCameraPublic(RotationDirection _Direction)
        {
            RotationDir = _Direction;
        }

        //
        void SmoothSnapToSelectedAnimal(FollowState _State)
        {
            if (FollowingAnimal == null)
                return;

            if (_State == FollowState.ReturningToAnimal)
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(FollowingAnimal.transform.position.x, 0 ,FollowingAnimal.transform.position.z), SnapSpeed);

            if(Vector3.Distance(transform.position, FollowingAnimal.transform.position) < 0.6f)
                CameraState = FollowState.FollowingAnimal;
        }

        //
        void ResetValues()
        {
            Movement = new Vector2();
            RotationDir = RotationDirection.Null;
            ZoomDelta = 0;
        }
    }
}