﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionPulse : MonoBehaviour {

    public enum ScaleDirection
    {
        Expand,
        Shrink
    }

    public float PulseScale = 0.1f;
    private float DecreasePerMilisecond;
    private float CurrentTime;
    private ScaleDirection CurrenDirection = ScaleDirection.Shrink;

    private void Start()
    {
        DecreasePerMilisecond = PulseScale / 10;
    }

    // Update is called once per frame
    void Update () {
        CurrentTime += Time.deltaTime;
        float Amount = CurrentTime * DecreasePerMilisecond;

        if (CurrentTime > 1)
        {
            switch (CurrenDirection)
            {
                case ScaleDirection.Expand:
                    CurrenDirection = ScaleDirection.Shrink;
                    CurrentTime = 0;
                    break;
                case ScaleDirection.Shrink:
                    CurrenDirection = ScaleDirection.Expand;
                    CurrentTime = 0;
                    break;
            }
        }

        switch (CurrenDirection)
        {
            case ScaleDirection.Shrink:
                transform.localScale = new Vector3(transform.localScale.x - Amount, transform.localScale.y, transform.localScale.z - Amount);
                break;
            case ScaleDirection.Expand:
                transform.localScale = new Vector3(transform.localScale.x + Amount, transform.localScale.y, transform.localScale.z + Amount);
                break;
        }

    }
}
