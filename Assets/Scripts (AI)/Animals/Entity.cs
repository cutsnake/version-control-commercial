﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class Entity : MonoBehaviour
    {
        public Node OccupiedNode;

        protected virtual void Start()
        {

        }

        protected void FindOccupiedNode()
        {
            OccupiedNode = NodeManager.Instance.FindNodeFromWorldPosition(transform.position);
        }
    }
}
