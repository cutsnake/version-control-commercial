﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace AI
{
    public class Animal : Entity
    {
        public float Speed = 1f;
        public float CurrentSpeed;
        public float RotationSpeed = 4f;
        //public AudioSource noises;

        //This will be determined by doubling the Animal's collider width.
        public float ColliderDistance = 1f;
        public AnimalStatus CurrentState = AnimalStatus.Idle;
        public AnimalTypes AnimalType = AnimalTypes.Null;

        // Use this for initialization
        protected override void Start()
        {
            CurrentSpeed = Speed;
            EventManager.InvokeEntityReg(this);
            //noises = gameObject.GetComponent<AudioSource>();
            //noises.Play();
        }

        // Update is called once per frame
        void Update()
        {

        }

        //
        public void RequestDestroy(bool _Destroy)
        {
            if(_Destroy)
            {
                EventManager.InvokeEnityDeReg(this);
                Destroy(gameObject);
            }
        }
        
        void TrackOccupiedNode()
        {

        }
    }
}
