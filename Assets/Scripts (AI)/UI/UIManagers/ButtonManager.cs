﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace AI
{
    /// <Notes>
    /// Quick, dirty, inefficent code that gets stuff working
    /// </Notes>

    public class ButtonManager : MonoBehaviour
    {
        public CameraTraverse ThisCamTraverse;
        public SkillButton[] SkillButtons = new SkillButton[4];
        public Vector2 CurrentMovementInput;
        public bool ButtonIsBeingOveredOver;
        public SkillSelection CurrentSelectedSkill = SkillSelection.Null;

        FlockData UIFlockData;

        // Use this for initialization
        void Start()
        {
            StartCoroutine(Wait());
            if (ThisCamTraverse == null)
                ThisCamTraverse = Camera.main.GetComponentInParent<CameraTraverse>();

            SubToEvents();
            UpdateSkillButtons(UIFlockData);

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void LateUpdate()
        {
            if (ThisCamTraverse.InputState == CameraInputState.ButtonInput)
            {
                ThisCamTraverse.MoveCameraPublic(CurrentMovementInput);
            }
            CurrentMovementInput = Vector2.zero;
        }

        public void ReciveMovemetInput(Vector2 _Input)
        {
            CurrentMovementInput += _Input;
        }
        public void ReciveReturnToAnimalInput()
        {
            ThisCamTraverse.ChangeState(FollowState.ReturningToAnimal);
           EventManager.InvokeButtonSelectedUpdate(false);
        }
        public void ReciveRotationInput(RotationDirection _Direction)
        {
            ThisCamTraverse.RotateCameraPublic(_Direction);
        }
        public void ReviveFlockData(FlockData _Data)
        {
            UIFlockData = _Data;
            UpdateSkillButtons(UIFlockData);
        }
        public void UpdateSkillSelection(SkillSelection _Selection)
        {
            CurrentSelectedSkill = _Selection;
            if (_Selection == SkillSelection.Null)
                EventManager.InvokeButtonSelectedUpdate(false);
            EventManager.InvokeSkillButtonSelection(_Selection);
        }


        void UpdateSkillButtons(FlockData _Data)
        {
            for(int ButtonIndex = 0; ButtonIndex < SkillButtons.Length; ++ButtonIndex)
                SkillButtons[ButtonIndex].SetButton(SkillSelection.Null);

            if (_Data == null)
                return;

            int SkillIndex = 0;
            if(_Data.CowsInFlock > 0)
            {
                SkillButtons[SkillIndex].SetButton(SkillSelection.Push);
                ++SkillIndex;
                SkillButtons[SkillIndex].SetButton(SkillSelection.RemoveHeavy);
                ++SkillIndex;
            }
            if(_Data.ChickensInFlock > 0)
            {
                SkillButtons[SkillIndex].SetButton(SkillSelection.RemoveLight);
                ++SkillIndex;
            }
            if(_Data.PigsInFlock > 0)
            {
                SkillButtons[SkillIndex].SetButton(SkillSelection.Dig);
                ++SkillIndex;
            }

            print(SkillIndex);
        }


        //
        void SubToEvents()
        {
            EventManager.FlockDataReg += ReviveFlockData;
            EventManager.ClearSkillSelection += ClearSkillSelection;
        }
        //
        public void ClearSkillSelection()
        {
            UpdateSkillSelection(SkillSelection.Null);
        }

        IEnumerator Wait()
        {
            yield return new WaitForSeconds(2);
        }
    }
}

