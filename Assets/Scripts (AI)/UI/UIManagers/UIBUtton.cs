﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

namespace AI
{
    public class UIBUtton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
    {
        public ButtonManager BManager;
        public bool ToggleActivation;

        public void OnPointerEnter(PointerEventData eventData)
        {
            BManager.ButtonIsBeingOveredOver = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            BManager.ButtonIsBeingOveredOver = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (ToggleActivation == false)
                EventManager.InvokeButtonSelectedUpdate(true);
            ToggleActivation = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (ToggleActivation == true)
                EventManager.InvokeButtonSelectedUpdate(false);
            ToggleActivation = false;
        }

        // Use this for initialization
        protected virtual void Start()
        {
            if (BManager == null)
            {
              
                BManager = GetComponentInParent<ButtonManager>();
            }
        }
    }
}
