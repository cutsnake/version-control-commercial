﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

	public int animalnumber;
	public Text AnimalCount;
	public List<GameObject> CountOfAnimals;
    public Scene scene;

	// Use this for initialization
	void Start () {
		animalnumber = new List<GameObject>(GameObject.FindGameObjectsWithTag("AllAnimals")).Count;
		
		AnimalCount.text = "Animal Count: " + animalnumber.ToString();
        
	}
	
	// Update is called once per frame
	void Update () {
		if (animalnumber == 0)
		{
            StartCoroutine(Changescene());
            
        
		}
	}

	public void updateUiCount()
	{
		animalnumber -= 1;
		AnimalCount.text = "Animal Count: " + animalnumber.ToString();
	}

	//public void EndGameState()
	//{

 //       int scene = SceneManager.GetActiveScene().buildIndex;
 //       SceneManager.LoadScene(scene,LoadSceneMode.Single );
			
		
	//}

    IEnumerator Changescene()
    {
        yield return new WaitForSeconds(1);

        //SceneManager.LoadScene(1);
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
