﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace AI
{
    

    public class SkillButton : UIBUtton, IPointerDownHandler, IPointerUpHandler
    {
        public SkillSelection SetSkill = SkillSelection.Null;
        public Button ThisButton;
        public Image ButtonImage;
        public Text ButtonText;

        protected override void Start()
        {
            StartCoroutine(Wait());
            base.Start();
            ThisButton = GetComponent<Button>();
            ButtonImage = GetComponent<Image>();
            ButtonText = GetComponentInChildren<Text>();
        }

        public void ToggleSelection()
        {
            if (BManager.CurrentSelectedSkill != SetSkill)
            {
                BManager.UpdateSkillSelection(SetSkill);
                //EventManager.InvokeButtonSelectedUpdate(true);
            }
            else if(BManager.CurrentSelectedSkill == SetSkill && BManager.CurrentSelectedSkill != SkillSelection.Null)
            {
                BManager.UpdateSkillSelection(SkillSelection.Null);
                //EventManager.InvokeButtonSelectedUpdate(false);
            }
        }

        public void SetButton(SkillSelection _SetSkill)
        {
            switch(_SetSkill)
            {
                case SkillSelection.Null:
                    print("no");
                    if (ThisButton == null)
                        break;
                    this.gameObject.SetActive(false);
                    break;
                case SkillSelection.Push:
                    print("push");
                    if (ButtonText == null)
                        break;
                    ButtonText.text = "Push";
                    SetSkill = _SetSkill;
                    if (ThisButton == null)
                        break;
                    this.gameObject.SetActive(true);
                    break;
                case SkillSelection.RemoveHeavy:
                    print("kickl");
                    if (ButtonText == null)
                        break;
                    ButtonText.text = "Kick";
                    SetSkill = _SetSkill;
                    if (ThisButton == null)
                        break;
                    this.gameObject.SetActive(true);
                    break;
                case SkillSelection.RemoveLight:
                    print("peck");
                    if (ButtonText == null)
                        break;
                    ButtonText.text = "Peck";
                    SetSkill = _SetSkill;
                    if (ThisButton == null)
                        break;
                    this.gameObject.SetActive(true);
                    break;
                case SkillSelection.Dig:
                    print("dig");
                    if (ButtonText == null)
                        break;
                    ButtonText.text = "Dig";
                    SetSkill = _SetSkill;
                    if (ThisButton == null)
                        break;
                    this.gameObject.SetActive(true);
                    break;
            }
        }

        IEnumerator Wait()
        {
            yield return new WaitForSeconds(2);
        }
    }
}
