﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AI
{
    public class RotationButton : UIBUtton, IPointerDownHandler, IPointerUpHandler
    {
        public RotationDirection DirectionOfRoation = RotationDirection.Null;

        private void Update()
        {
            GiveInputToButtonManager();
        }

        void GiveInputToButtonManager()
        {
            if (!ToggleActivation)
                return;

            Vector2 Input = Vector2.zero;
            switch (DirectionOfRoation)
            {
                case RotationDirection.Clockwise:
                    BManager.ReciveRotationInput(RotationDirection.Clockwise);
                    break;
                case RotationDirection.AntiClockwise:
                    BManager.ReciveRotationInput(RotationDirection.AntiClockwise);
                    break;
            }
        }
    }
}