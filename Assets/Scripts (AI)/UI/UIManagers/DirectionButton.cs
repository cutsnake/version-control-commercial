﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AI
{
    public class DirectionButton : UIBUtton, IPointerDownHandler, IPointerUpHandler
    {
        public MovementDirection DirectionOfMovement = MovementDirection.Null;

        private void Update()
        {
            GiveInputToButtonManager();
        }

        void GiveInputToButtonManager()
        {
            if (!ToggleActivation)
                return;

            Vector2 Input = Vector2.zero;
            switch(DirectionOfMovement)
            {
                case MovementDirection.North:
                    Input = new Vector2(0,1);
                    break;
                case MovementDirection.South:
                    Input = new Vector2(0, -1);
                    break;
                case MovementDirection.East:
                    Input = new Vector2(1, 0);
                    break;
                case MovementDirection.West:
                    Input = new Vector2(-1, 0);
                    break;
            }
            BManager.ReciveMovemetInput(Input);
        }
    }
}
