﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace AI
{
    [CustomEditor(typeof(RemovableObject))]
    public class RemoveObjectInspector : Editor
    {
        public GameObject IndicatorPrefab;
        RemovableObject TargetRemoveObject;
        GameObject PushPosiitonIndicator;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            TargetRemoveObject = (RemovableObject)target;
            if (TargetRemoveObject != null)
            {
                GameObject POA = TargetRemoveObject.GetIndicators();
                if (POA != null)
                    PushPosiitonIndicator = POA;
            }

            if (TargetRemoveObject != null && PushPosiitonIndicator != null)
            {
                TargetRemoveObject.RemovePosition = PushPosiitonIndicator.transform.position;
                PushPosiitonIndicator.transform.SetParent(TargetRemoveObject.transform);
            }

            GUILayout.Space(10);
            if (GUILayout.Button("Spawn/Reset Indicator"))
            {
                AddInteractionPositions(IndicatorPrefab, TargetRemoveObject);
            }
            if (GUILayout.Button("Set Remove Position"))
            {
                if (PushPosiitonIndicator == null)
                    return;

                TargetRemoveObject.RemovePosition = PushPosiitonIndicator.transform.position;
                PushPosiitonIndicator.transform.SetParent(TargetRemoveObject.transform);
            }
        }

        public void AddInteractionPositions(GameObject _IndicatorPrefab, RemovableObject _PushTarget)
        {
            if (PushPosiitonIndicator != null)
                return;
            else if (PushPosiitonIndicator != null)
                DestroyIndecators();

            float BoundsX = 0;

            PushPosiitonIndicator = Instantiate(IndicatorPrefab);
            PushPosiitonIndicator.name = "Push Start Position";
            PushPosiitonIndicator.transform.SetParent(_PushTarget.transform);

            Collider ThisFoundCol = _PushTarget.gameObject.GetComponent<Collider>();
            if (ThisFoundCol == null)
            {
                ThisFoundCol = _PushTarget.gameObject.GetComponentInParent<Collider>();
                if (ThisFoundCol == null)
                    _PushTarget.gameObject.GetComponentInChildren<Collider>();
            }

            if (ThisFoundCol != null)
            {
                BoundsX = ThisFoundCol.bounds.size.x;
            }

            _PushTarget.RemovePosition = _PushTarget.transform.position + new Vector3(BoundsX * 2 + 1f, -_PushTarget.transform.position.y, 0);
            PushPosiitonIndicator.transform.position = _PushTarget.RemovePosition;

            Material NewMat = new Material(PushPosiitonIndicator.GetComponent<Renderer>().sharedMaterial);
            NewMat.color = Color.blue;
            PushPosiitonIndicator.GetComponent<Renderer>().material = NewMat;

            _PushTarget.SetIndicators(PushPosiitonIndicator);
        }

        void DestroyIndecators()
        {
            DestroyImmediate(PushPosiitonIndicator);
        }
    }
}
