﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace AI
{
    [CustomEditor(typeof(PushObject))]
    public class PushObjectInspector : Editor
    {
        public GameObject IndicatorPrefab;
        PushObject TargetPushObject;
        GameObject PushPosiitonIndicator;
        GameObject PushEndPosIndicator;
        Vector3 RaisePos = Vector3.zero;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            TargetPushObject = (PushObject)target;
            if(TargetPushObject != null)
            {
                GameObject[] POA = TargetPushObject.GetIndicators();
                if(POA != null)
                {
                    PushPosiitonIndicator = POA[0];
                    PushEndPosIndicator = POA[1];
                }
            }

            if(TargetPushObject!= null && PushEndPosIndicator != null && PushPosiitonIndicator != null)
            {
                TargetPushObject.PushEndPos = PushEndPosIndicator.transform.position;
                TargetPushObject.PushPosition = PushPosiitonIndicator.transform.position;
                PushPosiitonIndicator.transform.SetParent(TargetPushObject.transform);
                PushEndPosIndicator.transform.SetParent(TargetPushObject.transform);
            }

            GUILayout.Space(10);
            if (GUILayout.Button("Spawn/Reset Indicators"))
            {
                AddInteractionPositions(IndicatorPrefab, TargetPushObject);
            }
            if(GUILayout.Button("Set Push Positions"))
            {
                if (PushEndPosIndicator == null && PushPosiitonIndicator == null)
                    return;

                TargetPushObject.PushEndPos = PushEndPosIndicator.transform.position;
                TargetPushObject.PushPosition = PushPosiitonIndicator.transform.position;
                PushPosiitonIndicator.transform.SetParent(TargetPushObject.transform);
                PushEndPosIndicator.transform.SetParent(TargetPushObject.transform);
            }
        }

        public void AddInteractionPositions(GameObject _IndicatorPrefab, PushObject _PushTarget)
        {
            if (PushPosiitonIndicator != null && PushEndPosIndicator != null)
                return;
            else if (PushPosiitonIndicator != null || PushEndPosIndicator != null)
                DestroyIndecators();

            float BoundsX = 0;
            float BoundsZ = 0;

            PushPosiitonIndicator = Instantiate(IndicatorPrefab);
            PushPosiitonIndicator.name = "Push Start Position";
            PushPosiitonIndicator.transform.SetParent(_PushTarget.transform);
            PushEndPosIndicator = Instantiate(IndicatorPrefab);
            PushEndPosIndicator.name = "End Position";
            PushEndPosIndicator.transform.SetParent(_PushTarget.transform);

            Collider ThisFoundCol = _PushTarget.gameObject.GetComponent<Collider>();
            if (ThisFoundCol == null)
            {
                ThisFoundCol = _PushTarget.gameObject.GetComponentInParent<Collider>();
                if (ThisFoundCol == null)
                    _PushTarget.gameObject.GetComponentInChildren<Collider>();
            }

            if (ThisFoundCol != null)
            {
                BoundsX = ThisFoundCol.bounds.size.x;
                BoundsZ = ThisFoundCol.bounds.size.z;
                RaisePos = new Vector3(0, ThisFoundCol.bounds.extents.y, 0);
            }

            _PushTarget.PushPosition = _PushTarget.transform.position + new Vector3(BoundsX*2 + 1f, -_PushTarget.transform.position.y, 0);
            _PushTarget.PushEndPos = _PushTarget.transform.position + new Vector3(0, -_PushTarget.transform.position.y, BoundsZ*2 + 1f);

            PushPosiitonIndicator.transform.position = _PushTarget.PushPosition;
            PushEndPosIndicator.transform.position = _PushTarget.PushEndPos;

            PushEndPosIndicator.GetComponent<Renderer>().sharedMaterial.color = Color.red;
            Material NewMat = new Material(PushEndPosIndicator.GetComponent<Renderer>().sharedMaterial);
            NewMat.color = Color.green;
            PushPosiitonIndicator.GetComponent<Renderer>().material = NewMat;

            _PushTarget.SetIndicators(PushPosiitonIndicator, PushEndPosIndicator);
        }

        void DestroyIndecators()
        {
            DestroyImmediate(PushEndPosIndicator);
            DestroyImmediate(PushPosiitonIndicator);
        }

        private void OnDrawGizmos()
        {
            if (PushPosiitonIndicator != null && PushEndPosIndicator != null && TargetPushObject != null)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(TargetPushObject.transform.position + RaisePos, PushEndPosIndicator.transform.position + RaisePos);
            }
        }
    }
}
