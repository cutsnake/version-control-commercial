﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace AI
{
    [CustomEditor(typeof(DigableObject))]
    public class DigObjectInspector : Editor
    {

        public GameObject IndicatorPrefab;
        DigableObject TargetDiggableObject;
        GameObject DigStartPosiitonIndicator;
        GameObject DigEndPosIndicator;
        Vector3 RaisePos = Vector3.zero;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            TargetDiggableObject = (DigableObject)target;
            if (TargetDiggableObject != null)
            {
                GameObject[] POA = TargetDiggableObject.GetIndicators();
                if (POA != null)
                {
                    DigStartPosiitonIndicator = POA[0];
                    DigEndPosIndicator = POA[1];
                }
            }

            if (TargetDiggableObject != null && DigEndPosIndicator != null && DigStartPosiitonIndicator != null)
            {
                DigStartPosiitonIndicator.transform.SetParent(TargetDiggableObject.transform);
                DigEndPosIndicator.transform.SetParent(TargetDiggableObject.transform);
            }

            GUILayout.Space(10);
            if (GUILayout.Button("Spawn/Reset Indicators"))
            {
                AddInteractionPositions(IndicatorPrefab, TargetDiggableObject);
            }
            if (GUILayout.Button("Set Dig Positions"))
            {
                if (DigEndPosIndicator == null && DigStartPosiitonIndicator == null)
                    return;

                DigStartPosiitonIndicator.transform.SetParent(TargetDiggableObject.transform);
                DigEndPosIndicator.transform.SetParent(TargetDiggableObject.transform);
            }
        }

        public void AddInteractionPositions(GameObject _IndicatorPrefab, DigableObject _DigTarget)
        {
            if (DigStartPosiitonIndicator != null && DigEndPosIndicator != null)
                return;
            else if (DigStartPosiitonIndicator != null || DigEndPosIndicator != null)
                DestroyIndecators();

            float BoundsX = 0;
            float BoundsZ = 0;

            DigStartPosiitonIndicator = Instantiate(IndicatorPrefab);
            DigStartPosiitonIndicator.name = "Dig Start Position";
            DigStartPosiitonIndicator.transform.SetParent(_DigTarget.transform);
            DigEndPosIndicator = Instantiate(IndicatorPrefab);
            DigEndPosIndicator.name = "Dig Exit Position";
            DigEndPosIndicator.transform.SetParent(_DigTarget.transform);

            DigStartPosiitonIndicator.transform.position = _DigTarget.transform.position + new Vector3(BoundsX * 2 + 1f, -_DigTarget.transform.position.y, 0);
            DigEndPosIndicator.transform.position = _DigTarget.transform.position + new Vector3(0, -_DigTarget.transform.position.y, BoundsZ * 2 + 1f);


            DigEndPosIndicator.GetComponent<Renderer>().sharedMaterial.color = Color.red;
            Material NewMat = new Material(DigEndPosIndicator.GetComponent<Renderer>().sharedMaterial);
            NewMat.color = Color.green;
            DigStartPosiitonIndicator.GetComponent<Renderer>().material = NewMat;

            _DigTarget.SetIndicators(DigStartPosiitonIndicator, DigEndPosIndicator);
        }

        void DestroyIndecators()
        {
            DestroyImmediate(DigEndPosIndicator);
            DestroyImmediate(DigStartPosiitonIndicator);
        }

        private void OnDrawGizmos()
        {
            if (DigStartPosiitonIndicator != null && DigEndPosIndicator != null && TargetDiggableObject != null)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(TargetDiggableObject.transform.position + RaisePos, DigEndPosIndicator.transform.position + RaisePos);
            }
        }
    }
}