﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace AI
{
    [CustomEditor(typeof(DogFlock))]
    public class FlockingTestINspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DogFlock TargetNM = (DogFlock)target;

            GUILayout.Space(10);
            if (GUILayout.Button("Test CIrcle Radius"))
            {
                TargetNM.SetAllFlockPositions();
            }
        }
    }
}
