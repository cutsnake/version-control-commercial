﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace AI
{
    [CustomEditor(typeof(NodeManagerSetup))]
    public class NodeManagerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            NodeManagerSetup TargetNM = (NodeManagerSetup)target;

            GUILayout.Space(10);
            if (GUILayout.Button("SetUpGrid"))
            {
                TargetNM.CallInitialSetup();
            }
            if (GUILayout.Button("Save Map Grid"))
            {
                TargetNM.CallSaveGridMap();
            }
            if (GUILayout.Button("Rest Tile Gid"))
            {
                TargetNM.ResetGrid();
            }

        }
    }
}
