﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOthers : MonoBehaviour {

    
    public Transform endposition;
    public bool activated;
	// Use this for initialization
	void Start ()
    {
        activated = false;

    }
	
	// Update is called once per frame
	void Update ()
    {
        
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        // somehow make it so all animals in dogs flock also get teleported rather than rely on allanimal tags
       if (collision.transform.tag == "AllAnimals" && activated == true)
        {
            Vector3 GotVec = endposition.transform.position;
            collision.transform.position = new Vector3(GotVec.x, 0, GotVec.z);
        }

        if (collision.transform.tag == "Player"  && activated == true)
        {
            Vector3 GotVec = endposition.transform.position;
            collision.transform.position = new Vector3(GotVec.x, 0, GotVec.z);
            
        }
    }
}
